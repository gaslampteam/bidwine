<?php

return [
    'service' => 'Recaptcha', // options: Recaptcha / Hcaptcha
    'sitekey' => env('CAPTCHA_SITEKEY', '6LeZj-QZAAAAAMffLDX5nvBYF-4tMkgDtIzUFnQW'),
    'secret' => env('CAPTCHA_SECRET', '6LeZj-QZAAAAAJ6nuT6MA4LilKh_OzdNU2901WDS'),
    'forms' => ['contact'],
    'error_message' => 'Captcha failed.',
    'disclaimer' => '',
    'invisible' => false,
    'hide_badge' => false,
    'enable_api_routes' => false,
];
