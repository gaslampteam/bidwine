---
title: 'Glen Breton Supports Wounded Warriors Canada'
news_image: news_images/SOE+80+2020.jpg
news_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Beverage International Distributors has just received the Glen Breton SOE Whisky, and is now available for all liquor stores in Alberta to order in for Christmas!'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Visit our product page, or view the entire line-up of Glenora Distillery products.'
  -
    type: set
    attrs:
      values:
        type: button_set
        align: left
        button_size: lg
        rounded: false
        roundness: null
        button_text: 'SOE Whisky Product Page'
        button_link: 'entry::996f09e5-59e5-4d9f-9c7c-f8a8472a85e4'
        color: null
        text_color: null
        outline: null
        target: false
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: '2020 marks the 80th anniversary of the creation of the Special Operations Executive (SOE).'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The legendary SOE was created on July 22, 1940. It was a secret British WWII organization whose purpose was to conduct espionage, sabotage and reconnaissance in occupied Europe. Few people were aware of SOE''s existence. Its agents were mainly tasked with sabotage and subversion behind enemy lines.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The Canadian connection to the SOE was derived both in terms of the Canadian personnel recruited to serve and the contribution of a training establishment in the form of Camp X. The site of Camp X, situated on the shores of Lake Ontario, is designated as Intrepid Park and named after a key figure in the establishment of the SOE, Canadian Sir William Stephenson (code named "Intrepid").'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The tartan on the box has been named after Sir William Stephenson and is aimed at honouring the legacy of the SOE and those who serve. Colours: tan pays homage to Canadian Special Operations Forces; green is a tribute to Canadian Intelligence; black signifies the history of covert operations; red signifies Canada and the sacrifice of the SOE; and blue pays tribute to the sky and sea which supports SOE operations.'
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607451235
news_categories:
  - whisky
  - glenora-distillery
id: 59755cbb-50ca-417d-a91e-cc2d7c5662b5
---
