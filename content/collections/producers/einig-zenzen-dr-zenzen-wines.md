---
title: 'Einig Zenzen "Dr. Zenzen" Wines'
tag_line: 'Your partner in wine'
producer_logo: producer_logos/dr-zhenzhen.png
producer_link: 'https://www.einig-zenzen.de/en/'
producer_bard_field:
  -
    type: set
    attrs:
      values:
        type: image_set
        image_link: null
        image_link_tab: false
        content_pic: content_images/zenzen-gluten.jpg
        width: quarter
        float: left
        image_alt: 'Dr. Zenzen - Gluten Free Certification'
        image_title: null
        zoom-image: true
        frame: true
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 4
        align_text: left
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>The house of Dr Zenzen have since 1636 been wine growers in Germany. This family tradition of more than 380 years stands for great experience and exclusiveness concerning the quality of wines. The Einig-Zenzen family produces wine grown in the hills above the river Mosel in the small picturesque village of Valwig in the heart of the Mosel wine region.</strong></p><p><strong>You can enjoy all of Dr. Zenzen wines knowing that they are all certified gluten free.</strong></p>'
  -
    type: set
    attrs:
      values:
        type: video_set
        video: 'https://www.youtube.com/watch?v=ajrzAlaCAZc'
        width: two-third
        float: center
  -
    type: paragraph
featured_producer: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605555615
id: 24e0d905-5046-432d-ad2e-483883b70204
---
