---
title: 'H & H Madeira'
producer_logo: producer_logos/HH-web.png
producer_link: 'https://henriquesehenriques.pt/?lang=en'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>Madeira wine is a still, fortified, blended wine that is wonderfully concentrated</strong><br><strong>and complex. The Portuguese island of Madeira is called the Island of the Immortals in</strong><br><strong>reference to it&rsquo;s oldest and most prestigious wines. H &amp; H is now the</strong><br><strong>largest independent producer and shipper of Madeira Wines and the only Madeira</strong><br><strong>House who owns vineyards. Madeira is a fortified wine, the shelf life of an open bottle is two years.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603757065
id: e64924b1-2005-4fb2-b046-1a18a1a34733
---
