---
title: 'Domaine Lucien Arkas'
producer_logo: producer_logos/Lukas-web.png
producer_link: 'http://www.lawines.com.tr/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>Everything starts with a dream. This is Aegean Turkey, the homeland of Mythology and Dionysus. Domaine Lucien Arkas manages the first organic vineyard certified by Ecocert in Turkey. An exceptional terroir spread over 200 hectares and blessed by the gentle Aegean&nbsp;Climate, Priority is given to manual harvesting at night in order to maintain the quality of the grapes. A perfect balance between tradition and modernism allows the wines to&nbsp;Age in French &amp; American oak barrels or stainless steel tanks.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603743860
id: 069c1e10-7c64-4ff9-918c-ac699839693b
---
