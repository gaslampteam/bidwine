---
title: 'Glenora Distillery'
producer_logo: producer_logos/Glen-web.png
producer_link: 'https://www.glenoradistillery.com/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>North America&rsquo;s First Single Malt Whisky Distillery. In the early 1800&rsquo;s, Scottish immigrants chose Cape Breton Island for their new home as its beauty so resembled the Highlands and Islands of Scotland. Many tradition and secrets came with these pioneers. The Making of a spirited whisky was one of them. The crystal&nbsp; clear water flows from the Maclellan&rsquo;s Brook Mabou Highlands Glenora Distillery in the heart of Nova Scotia, giving Glen Breton it&rsquo;s classic pure taste.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603743673
id: 71027d16-ef68-4f34-bbe8-c7a6a1e93564
---
