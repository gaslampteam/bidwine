---
title: 'ZD Wines'
tag_line: 'Committed to consistency, quality and style.'
producer_logo: producer_logos/ZD-weeb.png
producer_link: 'https://www.zdwines.com/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>One of Napa&rsquo;s Premium wineries the ZD Wine Family is owned and operated by three generations of the deLeuze family, located in the heart of Napa Valley. Founded in 1969. the ZD wine brand highlights the initials of the founding partners&rsquo; last name, but it also symbolizes the term Zero Defects, an acronym for their quality control program. ZD winery is a three-time award winner of the prestigious Decanter Award at the international Wine competition </strong><br><strong>in London. In 1987 the winery narrowed its focus to three varietals: Chardonnay , Cabernet Sauvignon and Pinot Noir.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603744018
id: 4ffba8cc-6cb9-4b25-9ee1-9afb68d98861
---
