---
title: 'Alde Gott'
producer_logo: producer_logos/gott-logo-1.png
producer_link: 'https://www.aldegott.de/en/home.html'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>Alde Gott, established in 1948 has it&rsquo;s 580 acres of vineyards shared by 415</strong><br><strong>members of a co-operative. Although the area is a Riesling stronghold, the co-operative has</strong><br><strong>made a name for itself with a red Spatburgunder (Pinot Noir) from&nbsp; Sasbachwalder Adle</strong><br><strong>Gott side, of which 60% is planted with the Spatburgunder.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603758259
id: dd98500c-3e4d-4cef-be65-8183cfa8e4dc
---
