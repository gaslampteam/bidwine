---
title: 'Our wine, YOUR Brand'
tag_line: 'Custom brand our quality wine with your own label.'
producer_logo: producer_logos/BID_logo_cropped.png
producer_bard_field:
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: left
        left_border: false
        right_border: false
        p-lead_set: '<p>A select blend of the highest quality grapes sourced from France. Grab the opportunity to have a house wine which can now represent your own brand,logo, or special occasion.</p>'
  -
    type: set
    attrs:
      values:
        type: heading_set
        heading_text: ' Our wine... YOUR brand'
        margin_top: '2'
        margin_bottom: '5'
        heading_size: h2
        header_looks_like: itself
        heading-float: center
        heading_color: 0
  -
    type: set
    attrs:
      values:
        type: image-gallery_set
        columns: '6'
        frame: true
        zoom-image: true
        gallery:
          - 'product-images/Custom Label 10.jpg'
          - 'product-images/Custom Label 11.jpg'
          - 'product-images/Custom Label 2.jpg'
          - 'product-images/Custom Label 3.jpg'
          - 'product-images/Custom Label 4.jpg'
          - 'product-images/Custom Label 5.jpg'
          - 'product-images/Custom Label 6.jpg'
          - 'product-images/Custom Label 7.jpg'
          - 'product-images/Custom Label 8.jpg'
          - 'product-images/Custom Label 9.jpg'
          - 'product-images/Custom Red House.jpg'
          - 'product-images/Custom White House.jpg'
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1620425018
id: d0769c78-9209-4323-9bc8-6d89aae759a8
---
