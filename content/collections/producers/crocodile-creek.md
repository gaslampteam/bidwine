---
title: 'Crocodile Creek'
tag_line: 'A True Australian Original!'
producer_logo: producer_logos/crock-web.png
producer_bard_field:
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>A true Australian original. An amazing line up of&nbsp; fruit-forward concentrated wines.</strong></p>'
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603757347
id: 0559ace2-dfc9-465c-ba08-c71765ef2350
---
