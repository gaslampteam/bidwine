---
id: dfe327fa-39ee-4671-bea2-f089ad9ae2ba
blueprint: producers
title: 'Kloster Ebernach'
tag_line: '“It is our goal to preserve the Mosel-valley in all its beauty and uniqueness for current and future generations.“'
producer_logo: producer_logos/logo.jpg
producer_link: 'https://weingut-ebernach.de/'
producer_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        text: "A Cochem family winery with tradition is behind the Ebernach winery.\_The brothers Dr.\_Peter-Josef Zenzen and Heinz-Rudolf Zenzen have set themselves the goal of successfully continuing the long tradition of the Ebernach monastery winery.\_The viticulture on our premises looks back on almost 350 years of history."
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1627072306
---
