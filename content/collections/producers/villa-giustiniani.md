---
title: 'Villa Giustiniani'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>Winegrowers since 1559. Villa Giustiniani is produced in the heart of the Asolo DOCG production area, on the spectacular Montello hills. The deep, stony, glacial subsoils or &ldquo;Red Asolo&quot;, yield a mineral-rich, structured, and long-lived Prosecco. These vineyards enjoy a southwestern exposure and are ideally situated on a hillside near the Treviso plain where it is crossed</strong><br><strong>by the Piave River.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605041079
tag_line: 'Since 1559'
producer_logo: producer_logos/villa-button.png
producer_link: 'https://www.sagrivit.it/'
id: 5dbf81a6-9d61-483f-a936-1d30b0008484
---
