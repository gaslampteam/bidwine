---
title: Ripshot
producer_logo: producer_logos/Rip-Shot-Back-splash.png
producer_link: 'https://ripshot.co/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>The notion of Ripshot was hatched on a warm Canadian summer night between friends. As one returned with half spilled shot glasses, conversation turned into frustration as stories were shared about drinks being tampered with. We have taken what was an honest and compassionate idea to reduce spills and contamination to create the first fully tamperproof shot glass. As of 2017, Ripshot&trade; is Internationally Patent Pending.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603743440
id: e7e5a70f-c445-4c09-b8a0-b239702ee9c0
---
