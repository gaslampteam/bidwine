---
title: Kallfelz
producer_logo: producer_logos/keff-web.png
producer_link: 'https://www.kallfelz.de/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p>Family owned for over 110 years<strong>.&nbsp;The best Riesling wine maker in</strong><br><strong>Germany.</strong>&nbsp;Famous for Mosel Slate, which gives the wines their character and strength. Kellfelz<br>Wines have a very intense minerality, whilst being light, refreshing and fruity.</p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603757535
tag_line: 'The Best Reisling Wine maker in Germany'
id: 5640e002-4405-44ff-b3ec-913da87957f1
published: false
---
