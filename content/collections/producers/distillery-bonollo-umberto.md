---
title: 'Distillery Bonollo Umberto'
producer_logo: producer_logos/Bonollo-web.png
producer_link: 'https://www.bonollo.it/en/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>The distillery Bonollo Umberto was founded in 1908 by Giuseppe</strong><br><strong>Bonollo, four generations producing fine grappa. This superior Grappa is in a</strong><br><strong>league of it&rsquo;s own.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603758365
id: 8effceca-b041-4d00-9b72-4bca76e6cf08
---
