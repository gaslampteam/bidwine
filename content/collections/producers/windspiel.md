---
title: Windspiel
tag_line: 'Joy of experimentation'
producer_logo: producer_logos/Windspiel-Logo.jpg
producer_link: 'https://gin-windspiel.de/'
producer_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'What happens when friends get together to make their own spirits? In the best case, something like the Windspiel brand is created.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Sandra Wimmeler and Denis Lönnendonker wanted to do their own thing and came up with the idea of ​​making gin themselves. For this purpose they settled in the middle of the country and got it into their heads to capture the spirit of the Vulkaneifel in their spirits. In 2014 the foundation stone was laid for the now internationally successful Windspiel Manufactory.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'With Windspiel Gin and other spirits such as Windspiel Potato Vodka or Kraut & Knolle everything revolves around the potato. The venture began with their cultivation a good six years ago. The team is currently having potatoes grown in the Vulkaneifel, which are then processed into a mash as a raw material. Alcoholic fermentation is followed by distillation, and with products such as the Windspiel Premium Dry Gin, natural ingredients for flavoring find their way into the distillate. Here, too, the ties to the local area are evident, as botanicals from the region are used whenever possible.'
featured_producer: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1620424986
id: 91f470ee-0a15-4682-8a28-1d4a79197b26
---
