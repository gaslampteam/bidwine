---
id: 5558f663-9c89-4076-98ff-0cc9e182ecb3
published: false
blueprint: producers
title: 'St. Hubertus Estate Winery'
producer_logo: producer_logos/hub-web.png
producer_link: 'http://www.st-hubertus.bc.ca/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: "<p><strong>For over 25 years the Gebert family have tended the original Hughes Vineyards which are among the first Okanagan grape plantings in 1928. They produce 100% estate grown wines that reflect the terroir of the North Okanagan Valley. St Hubertus believe in the principles of organic &amp; sustainable farming. All St. St Hubertus Wines are VQA – Vinters Quality Alliance. A regulatory &amp; appellation\_system which guarantees high quality an authenticity of origin. 100% Canadian grapes.</strong></p>"
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1636263278
---
