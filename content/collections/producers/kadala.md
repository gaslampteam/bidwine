---
title: Kadala
producer_logo: producer_logos/kadala-web.png
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>Spain has a long history of producing fine wines. Spain has the largest</strong><br><strong>area of land dedicated to viticulture of any country in the world.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603757890
id: 19547a5d-9518-40fa-9e1d-73210c775dbe
---
