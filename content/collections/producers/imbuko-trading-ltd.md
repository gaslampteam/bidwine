---
title: 'Imbuko Trading Ltd.'
tag_line: 'Where Quality Meets Value'
producer_logo: producer_logos/imbuku-web.png
producer_link: 'https://www.imbuko.co.za/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>Imbuko wines are situated near Wellington the heart of the Boland Winelands.</strong><br><strong>Imbuko, means admiration in Xhosa.&nbsp;Xhosa serves as one of South Africa&rsquo;s eleven official</strong><br><strong>languages and the native tongue of Nelson Mandela. What further sets Imbuko apart is the fact that they are one of the few farms that cultivate rootstock, not only for their&nbsp;own use, but for the&nbsp;South Africa wine industry.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603744134
id: 21047df7-1bda-43ae-a54d-c1594901c112
---
