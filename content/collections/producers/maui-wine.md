---
title: 'MAUI Wine'
producer_logo: producer_logos/maui-web.png
producer_link: 'https://mauiwine.com/'
producer_bard_field:
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>Maui Wines formerly Tedeschi Winery is Hawaii&rsquo;s oldest winery and is set on the slopes of Upcountry Maui, 2000 feet above sea level, amid the vast, twenty-thousand Acre &ldquo; Ulupalakua Ranch. Twenty-two acres of gradually sloping volcanic grasslands are Used to cultivate the grapes. Established in 1974, while waiting for newly planted grapes to mature, our talented wine-makers tested their skills on a different juice fruit that defines Hawaii, THE PINEAPPLE.</strong></p>'
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603757160
id: 36f7c238-1c91-4ecd-ac5f-600a3921d2a5
---
