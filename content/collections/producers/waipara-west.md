---
title: 'Waipara West'
producer_logo: producer_logos/wap-webbfff.png
producer_link: 'http://waiparawest.com/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>A family owned vineyard located some 40 miles from Christchurch New Zealand,</strong><br><strong>situated on the north facing sun drenched&nbsp; Waipara River. At Waipara West they</strong><br><strong>do not use insecticides, herbicides or artificial fertilizer. Only hand &ndash; picked grapes from</strong><br><strong>their own vineyards are used for their wines. Described as more European in</strong><br><strong>Style than new world.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603756940
id: dab49389-1538-4cde-bdc1-1e6a2e4f40da
---
