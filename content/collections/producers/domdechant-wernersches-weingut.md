---
title: 'Domdechant Wernersches Weingut'
producer_logo: producer_logos/dommmm.png
producer_link: 'https://domdechantwerner.com/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>A family owned estate since 1780. Now in the 8th Generation. Hochheim&nbsp;</strong><br><strong>is the cornerstone of&nbsp; Rheingau for the finest Riesling. The estate was honored with</strong><br><strong>the State Honorary Award for the best Riesling collection in the Rheingau region,</strong><br><strong>winning gold medals at every annual competition in Rheingau.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1603758021
id: 29a411d1-742d-4482-b211-868098700f30
---
