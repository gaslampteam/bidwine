---
title: 'Distributed by Zenzen'
producer_link: 'https://www.einig-zenzen.de/en/'
producer_bard_field:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: p-lead_set
        background-fill: null
        text-color: null
        padding: 'None (default)'
        align_text: center
        left_border: false
        right_border: false
        p-lead_set: '<p><strong>For more than 380 years Einig-Zenzen&nbsp; has been dedicated to</strong><br><strong>the wine trade. Quality assurance starts at Einig-Zenzen with the very first</strong><br><strong>step being the careful selection of wine growers.</strong></p>'
  -
    type: paragraph
featured_producer: false
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1606874408
producer_logo: producer_logos/bott-by-zen-web.png
id: 6c539cdf-1a95-48b5-8664-010c26c74012
---
