---
title: 'Sauvignon Blanc'
product_producer: dab49389-1538-4cde-bdc1-1e6a2e4f40da
product-image: product-images/Binder1_Page_70_Image_0002.jpg
product_sheet: 'downloads/Sauvignon Blanc.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'International Wine & Spirits Competition 2015 -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Silver Medal'
      -
        type: text
        text: ' '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Wine Advocate - '
      -
        type: text
        marks:
          -
            type: italic
        text: '86 points'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' A family-owned vineyard located some 40 miles from Christchurch, situated on north-facing sun-drenched terraces along the Waipara River. At Waipara West, they do not use insecticides, residual herbicides, or artificial fertilizer. Only hand-picked grapes from their own vineyards are used for their wines. Described as more European in style than the New World.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Hand-picked Sauvignon Blanc grapes from both the upper and lower terrace vineyards. A tangy classic Kiwi.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' New Zealand'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Canterbury/Waipara'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 9418587710027'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '13.5 vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 1.60 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Displays ripe tropical gooseberry aromas with hints of green capsicum on the nose, peach flavors on the palate with a well-rounded finish. A wine that seems both rich and bone dry at the same time.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Sauvignon Blanc’s zing is a delightful complement to the fresh flavors of seafood, shellfish, and whitefish. On its own a mouth-watering aperitif.'
SKU:
  -
    product_variant: 750ml
    sku: '611350'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605301989
id: 2efe5b4e-37b1-47fe-a368-87e8ac2c219d
---
