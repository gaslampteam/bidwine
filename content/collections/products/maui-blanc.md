---
title: 'Maui Blanc'
product_producer: 36f7c238-1c91-4ecd-ac5f-600a3921d2a5
product-image: product-images/Binder1_Page_58_Image_0002.jpg
product_sheet: 'downloads/Maui Blanc.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'The King of Fruits'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Clever, Spirited, Perfected'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'MauiWines formerly Tedeschi Winery is Hawaii''s oldest winery and is set on the slopes of Upcountry Maui, 2000 feet above sea level, amid the vast, twenty-thousand acre `Ulupalakua Ranch’. Twenty-two acres of gradually sloping volcanic grasslands are used to cultivate the grapes. Established in 1974. While waiting for the newly planted grapes to mature, our talented wine-makers tested their skills on a different juice- the fruit that defines Hawaii, the pineapple.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: '100% Maui-grown gold pineapple'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: 'United States'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Hawaii-Maui'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: UPC
              -
                type: text
                text: ': 30052000017'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 11.5 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 19.1 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Additives: '
              -
                type: text
                text: 'contains sulfites as a preservative'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'This wine is layered with subtle pineapple aromas and polished with notes of lavender throughout. A fresh and lively palate filled with pleasant tropical tones presented throughout the finish delivers a crisp and playful wine.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'This wine can be served for all occasions and is a great compliment to poultry, seafood, and spicy dishes, adds a tropical flair to any meal.'
SKU:
  -
    product_variant: 750ml
    sku: '579656'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605294745
id: d9cdfebd-363d-465f-b04b-7c3fb5f289b7
---
