---
title: 'Superior Rich Malmsey 3 Year'
product-image: product-images/Binder1_Page_56_Image_0002.jpg
product_sheet: 'downloads/Malmsey 3 year.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Madeira wine is a still, fortified, blended wine that is wonderfully concentrated and complex. The Portuguese island of Madeira is called the Island of the Immortals in reference to its wines, which can hold-and improve- their quality over centuries. Henriques & Henriques is one the oldest and most prestigious of the wineries in Madeira. H & H is now the largest independent producer and shipper of Madeira Wines and the only Madeira House that owns vineyards.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Malmsey grape Aged three years in Oak Cask'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Portugal
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' the sub-tropical Atlantic island of Madeira some 800 km due south-west of Portugal'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 5601196010016'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 19 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '108.5 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Malmsey is an extremely rich and luscious wine with a fine bouquet. Raisiny-fruity, full-bodied, fragrant, very rich, and sweet. It is a rich pungent exotically-flavored wine with refreshing acidity.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Best savored as a dessert wine, with rich foods, chocolate, petit fours, or with coffee. Also chefs’ favorite for enhancing rich sauces. Wonderful in cooking.'
SKU:
  -
    product_variant: 750ml
    sku: '721384'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605295403
product_producer: e64924b1-2005-4fb2-b046-1a18a1a34733
id: 50d39139-6804-415f-8cfe-4d52dead14d3
---
