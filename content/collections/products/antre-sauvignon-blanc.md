---
title: 'Antre Sauvignon Blanc'
product_producer: 069c1e10-7c64-4ff9-918c-ac699839693b
product-image: product-images/Antre-Sauvignon-Blanc_Page_1_Image_0002.jpg
product_sheet: 'downloads/Antre Sauvignon Blanc.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold:'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'China Wine and Spirits Awards 2016'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Everything starts with a dream. This is Aegean Turkey, the homeland of Mythology and Dionysus…'
  -
    type: paragraph
    content:
      -
        type: text
        text: '…the birthplace of Domaine Lucien Arkas which inherited the fertility, diversity, and extraordinary nature of the Aegean Region. Our wines owe their dignity and vision to their owner: Lucien Arkas.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'His vision of the wine is decidedly qualitative. He realized the dream of producing the best wines in Turkey.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Domaine Lucien Arkas'
      -
        type: text
        text: ' manages the first organic vineyard certified by Ecocert in Turkey. An exceptional terroir spread over 200 hectares and blessed by the gentle Aegean climate. Priority is given to manual harvesting at night in order to maintain the quality of the grapes. A perfect balance between tradition and modernism allows the wines to age in French and American oak barrels or stainless steel tanks.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' An estate-bottled Organic Sauvignon Blanc hand-harvested at night to ensure the fruity aroma of the grapes.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Turkey'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 8698819984141'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '13.5 % alc. /vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '1.60gram of sugar per liter'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A beautifully balanced wine with crispy acidity, fruity flavor of yellow pepper, and pineapple. Aromas of ripe tropical fruits such as citrus, fresh acidity with an intense persistence in the mouth.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food suggestion:'
      -
        type: text
        text: ' Recommended with all fish dishes. Our labels are the depiction of the boats carrying wine to Europe, from the open seas to a safe harbor. Just like a glass of wine helps us unwind after a busy and stressful day.'
SKU:
  -
    product_variant: 750ml
    sku: '812399'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605039245
id: eac4efd1-d55c-40ce-8fd4-aec89283d257
---
