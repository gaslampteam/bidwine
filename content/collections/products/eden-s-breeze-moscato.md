---
title: 'Eden’s Breeze Moscato'
product_producer: 21047df7-1bda-43ae-a54d-c1594901c112
product-image: product-images/Binder1_Page_37_Image_0002.jpg
product_sheet: 'downloads/Eden Breeze Moscato.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Imbuko Wines are situated near Wellington in the heart of the Boland Winelands. Imbuko means “Admiration for…” in Xhosa, which serves as one of South Africa’s eleven official languages and the native tongue of Nelson Mandela. What further sets Imbuko apart is the fact that the farm is one of the few farms that cultivate rootstock, not only for its own use but for a bigger part of the South African wine industry. Thus, the Imbuko portfolio can boast the winemaker''s hand from rootstock to the vine and from vine to the wine.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: '100% Moscato'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: 'South Africa: Wellington-Boland Winelands region'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure: '
              -
                type: text
                text: 'Zork closure The advantages of the Zork-closure are: It’s a re-usable sparkling cork that you can put back in the bottle. If you drink one or two glasses you can put the cork back in and it will keep the wine sparkling for 4-5 days.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 6009801318882'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '6.5 %'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual sugar: '
              -
                type: text
                text: 45g/l
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'This tongue-tingling treat is seductively sweet with BIG aromas of peaches, pears, and apricots. The light effervescence wakes up your taste buds and leads to a crisp, flavorful finish. An easy-drinking white wine with a touch of sparkle and a hint of exotic. Now with the new ZORK closure – to keep product even fresher.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food suggestions:'
      -
        type: text
        text: ' It is a perfect match to start or end any meal or enjoy it on its own.'
SKU:
  -
    product_variant: 750ml
    sku: '740155'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605205815
id: 7b03d129-b359-4265-8643-6b56c5e20082
---
