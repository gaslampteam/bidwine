---
id: 28601119-c4b3-4b48-a401-b98e6fd2c222
blueprint: products
title: 'Mosel Cochemer Bischofsstuhl'
product_producer: dfe327fa-39ee-4671-bea2-f089ad9ae2ba
product-image: product-images/Kloster_Ebernach_Bischofsstuhl_Riesling_Spaetlese.jpg
product_sheet: downloads/Kloster---852979-Mosel-Cochemer-Bischofsstuhl-1627072768.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The brothers Dr Peter-Josef Zenzen and Heinz-Rudolf Zenzen have set themselves the goal of successfully continuing the long tradition of the Ebernach Monastery winery. The viticulture of the monastery goes back 350 years. Such a tradition needs experience-and the Zenzen brothers have it. The family is already in the 13th generation as winemaking.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Riesling Spatlese -Estate wine'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: 'Mosel- Bischofsstuhl'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 400 155 1030115'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 10.50%vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 33. g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Full-bodied with perfect exoticism, pleasant sweetness in perfect balance with a delicate acidity. A delightful harmonious balance between acidity, off-dry fruitiness and mineral elegance our Mosel wines are perfect for healthy and easy drinking. A combination you can find nowhere else.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Enjoy with all summer salads and white meat. Also a pleasure on its own.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Award:'
      -
        type: text
        text: ' Best of the Riesling 2020 and Gold Medal at the Berlin Wine Trophy'
SKU:
  -
    product_variant: 750ml
    sku: '852979'
    type: sku
    enabled: true
product_categories:
  - riesling
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1627072840
---
