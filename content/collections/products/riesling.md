---
id: d28a40e6-c1e6-46e7-b2aa-74a64d5eeb5c
published: false
blueprint: products
title: Riesling
product_producer: 5558f663-9c89-4076-98ff-0cc9e182ecb3
product-image: product-images/Binder1_Page_75_Image_0002-1607370342.jpg
product_sheet: 'downloads/St Hubertus Riesling.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Wine of the Year'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold - '
      -
        type: text
        text: 'National Wine Awards Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Okanagan Wine Festival BC Best of the Varietal'
  -
    type: paragraph
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'For over 25 years the Gebert family have tended the original Hughes Vineyards, which are among the first Okanagan grape plantings in 1928. They produce 100% estate-grown wines that reflect the terroir of the North Okanagan Valley. St Hubertus believe in the principles of organic & sustainable farming-No Pesticide -No Herbicide -No Fungicide'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: '100% Estate grown Riesling grapes'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality: '
      -
        type: text
        text: 'VQA –Vintners’ Quality Alliance Is a Regulatory & appellation system that guarantees high quality and authenticity of origin.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' British Columbia-North Okanagan Valley'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 625259103144'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 13.1 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 15.2 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Classically styled, this wine shows delicate floral aromas with hints of honey, apple, and lime. This Divine Riesling has attractive, complex, sweet, floral, honeysuckle, and tropical aromas. Suggestions of peach, apricot, citrus, sweet-tart, and off-dry flavors tantalize the palate. With excellent balance, it has a long, faintly crisp clean finish.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Have it with friends over a bowl of chicken curry or just sip it at your romantic lakeside picnic. Enjoy with white meats, seafood, or bold flavor dishes such as curry. A very versatile wine.'
SKU:
  -
    product_variant: 750ml
    sku: '345009'
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1636263353
---
