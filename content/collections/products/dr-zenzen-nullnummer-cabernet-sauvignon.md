---
id: 6ca54ba7-7199-4880-bdaf-935c8d8001f8
blueprint: products
title: 'Nullnummer Cabernet Sauvignon'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/non-alc-cabernet.jpg
product_sheet: downloads/Nullnummer-Cabernet.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The house of Dr Zenzen have since 1636 been wine growers in Germany. This family tradition of more 385 years stands for great experience and exclusiveness concerning the quality of wines.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Produced from 100% Cabernet Sauvignon grapes'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin :'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Rheinpfalz
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of closure : Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC : 4008005045381'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 0.0% vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Deep garnet red in color. Full-bodied and round on the palate with a great note of blackcurrant. Soft and velvety. A NON-ALCOHOL RED all the flavor and finesse but none of the calories or alcohol.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Pairing: '
      -
        type: text
        text: 'This alcohol- free Cabernet is a wonderful accompaniment to beef and game dishes, pasta with strong sauces or spicy cheese.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving:'
      -
        type: text
        text: ' Best served chilled.'
SKU:
  -
    product_variant: 750ml
    sku: '852968'
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1627067590
product_categories:
  - non-alcoholic
---
