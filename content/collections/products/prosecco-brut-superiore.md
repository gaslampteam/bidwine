---
title: 'Prosecco Brut Superiore'
product_producer: 5dbf81a6-9d61-483f-a936-1d30b0008484
product-image: product-images/Binder1_Page_65_Image_0001.jpg
product_sheet: 'downloads/Prosecco Brut.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Winegrowers since 1559. Villa Giustiniani is produced in the heart of the Asolo DOCG production area, on the spectacular Montello hills. The deep, stony, glacial era morainic subsoils or “Red Asolo, yield a mineral-rich, structured, and long-lived Prosecco. Vineyards enjoy southwestern exposure and are ideally situated on a hillside near the Treviso plain where it is crossed by the Piave River.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' 100 % Glera (Prosecco) grapes grown in the hills of Montello in Veneto Italy.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Italy'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Bottle:'
              -
                type: text
                text: ' 750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure:'
              -
                type: text
                text: ' Cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 80099240 12771'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 11.50% alc./vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual sugar:'
              -
                type: text
                text: ' 8.0 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' This sparkling wine is pale yellow in color with a fine and persistent perlage that denotes a youthful freshness. On the nose hints of acacia flowers and citrus clearly prevail. A distinct mineral note on the palate reflects the character of the terrain typical of the hills Montello.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Excellent with raw fish and seafood dishes.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serve:'
      -
        type: text
        text: ' Enjoy well chilled in a champagne flute.'
SKU:
  -
    product_variant: 750ml
    sku: '796703'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607369768
id: 6cf3e66a-a27f-4ffe-82f9-0c7f04d4652b
---
