---
title: 'Fine Old Madeira 5 Year'
product-image: product-images/Binder1_Page_57_Image_0002.jpg
product_sheet: 'downloads/Malmsey 5 year.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Wine Writer Tony Aspler has just one word for it…'
      -
        type: text
        marks:
          -
            type: italic
        text: '` Delicious’'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Madeira wine is a still, fortified, blended wine that is wonderfully concentrated and complex. The Portuguese island of Madeira is called the Island of the Immortals in reference to its wines, which can hold-and improve- their quality over centuries. Henriques & Henriques is one the oldest and most prestigious of the wineries in Madeira. H & H is now the largest independent producer and shipper of Madeira Wines and the only Madeira House that owns vineyards.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Malmsey grape Aged 5 years in Oak Cask'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Portugal
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' the sub-tropical Atlantic island of Madeira some 800 km due south-west of Portugal'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '5601196020053'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 19 % vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: 111.00g/l
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'An intensely concentrated and rich wine with a strong flavor of ripe fruit, raisins, and caramel. Raisin-fruity, full-bodied, fragrant, very rich, and sweet.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Ideal as an after-dinner drink or as an aperitif with lemon zest. Perfect with nuts, strong cheeses, dark chocolate, or rich desserts. Madeira is a fortified wine, the shelf life of an open bottle is two years. Serve in a sherry glass or white wine glass either chilled or at room temperature.'
SKU:
  -
    product_variant: 750ml
    sku: '736030'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605295424
product_producer: e64924b1-2005-4fb2-b046-1a18a1a34733
id: eb56efef-c22c-47b2-97c8-6b8c5230f206
---
