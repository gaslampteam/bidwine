---
id: 4e9a063c-a609-46da-b789-92d0ef317e6a
published: false
blueprint: products
title: 'Pinot Blanc'
product_producer: 5558f663-9c89-4076-98ff-0cc9e182ecb3
product-image: product-images/Binder1_Page_74_Image_0001-1607370286.jpg
product_sheet: 'downloads/St Hubertus Pinot Blanc.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '88 points -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Wine Enthusiast'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'National Wine Awards Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'All Canadian Wine Championship Intervin Gold & Best Value '
  -
    type: paragraph
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' For over 25 years the Gebert family have tended the original Hughes Vineyards, which are among the first Okanagan grape plantings in 1928. They produce 100% estate-grown wines that reflect the terroir of the North Okanagan Valley. St Hubertus believe in the principles of organic & sustainable farming-No Pesticide -No Herbicide -No Fungicide'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: '100% estate-grown Pinot Blanc grapes'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality:'
      -
        type: text
        text: ' VQA –Vintners’ Quality Alliance Is a Regulatory & appellation system that guarantees high quality and authenticity of origin.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' British Columbia- North Okanagan Valley'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 625259102154'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 12.6 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 8.9 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Very pleasant aromas of warm spice and light citrus with a floral note of violets and rose petal aromas on the nose. It is medium-bodied with a balance of peachy and citrus overtones.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Pairs with salmon, crab, prawns, or oysters for a great meal. Have it on the beach with a pot of steaming crab, salmon on a cedar plank, or in the mountain with some sausage, goat cheese, and farmer’s bread.'
SKU:
  -
    product_variant: 750ml
    sku: '344978'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607370303
---
