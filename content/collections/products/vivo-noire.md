---
title: 'Vino Noire'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Binder1_Page_28_Image_0002.jpg
product_sheet: 'downloads/Dr Zenzen Vino Noire.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: '#1 Selling German Red Wine in Western Canada'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The house of Dr. Zenzen has since 1636 been wine growers in Germany. This family tradition of more than 380 years stands for great experience and exclusiveness concerning the quality of wines. The Einig-Zenzen family produces wine grown in the hills above the river Mosel in the small picturesque village of Valwig in the heart of the Mosel wine region.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Produced from the Dornfelder grape which is called ‘Shooting Star’ among the German red wine grapes.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality: '
      -
        type: text
        text: 'QbA- German Quality red wine'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Rheinhessen
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml blue Noblesse bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure: '
              -
                type: text
                text: 'Stelvin closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'UPC :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: '750 ml - '
              -
                type: text
                text: '4008005 149591'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: '1.5 liter - '
              -
                type: text
                text: '408005 149782'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '11 % vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '22.3 g/L'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Gluten:'
              -
                type: text
                text: ' Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Smooth and harmonic fruity aromas, miles of cherries and berries, beautifully light but well-rounded finish.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Pairing: '
      -
        type: text
        text: 'Perfect with all types of game and dark meats as well as pasta/red sauces. For dessert, try with dark chocolate or chocolate cheesecake.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving:'
      -
        type: text
        text: ' Best served slightly chilled.'
SKU:
  -
    product_variant: 750ml
    sku: '710037'
    link_override: null
    type: sku
    enabled: true
  -
    product_variant: '1.5 L'
    sku: '737021'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607366537
id: 1dec20d7-2bb0-47fa-b6ef-e67093ece357
---
