---
id: b31c9100-3c03-4ada-b7ee-5eff3c80c9fa
blueprint: products
title: 'Mosel Pommerner Sonnenuhr'
product_producer: dfe327fa-39ee-4671-bea2-f089ad9ae2ba
product-image: product-images/Kloster_Ebernach_Sonnenuhr_Riesling_Kabinett_Trocken.jpg
product_sheet: downloads/Kloster---852978-Mosel-Pommerner-Sonnenuhr-1627072659.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The brothers Dr Peter-Josef Zenzen and Heinz-Rudolf Zenzen have set themselves the goal of successfully continuing the long tradition of the Ebernach Monastery winery. The viticulture of the monastery goes back 350 years. Such a tradition needs experience-and the Zenzen brothers have it. The family is already in the 13th generation as winemaking.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Riesling – Kabinett Trocken- Estate wine'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Mosel- Sonnenuhr'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 40055 1030122'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 11.0%vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 8.g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Light, fruity wine that fully reflects the steep slopes typical Mosel region. A delightful harmonious balance between acidity, off-dry fruitiness and mineral elegance our Mosel wines are perfect for healthy and easy drinking. A combination you can find nowhere else.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Enjoy with all summer salads and white meat. Also a pleasure on its own.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving:'
      -
        type: text
        text: ' Serve slightly chilled at 10-12 C'
SKU:
  -
    product_variant: 750ml
    sku: '852978'
    type: sku
    enabled: true
product_categories:
  - riesling
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1627072724
---
