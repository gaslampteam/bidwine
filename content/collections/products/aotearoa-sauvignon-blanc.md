---
title: 'Aotearoa - Sauvignon Blanc'
product_producer: 6c539cdf-1a95-48b5-8664-010c26c74012
product-image: product-images/Aotearoa-Sauvignon-Blanc_Page_1_Image_0002.jpg
product_sheet: 'downloads/Aotearoa Sauvignon Blanc.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The explosive flavors of New Zealand Sauvignon Blanc have dazzled wine critics throughout the world, setting the international benchmark for style. When Marlborough’s first Sauvignon Blanc vines were planted, no one could have predicted the superstar status that this variety would attain. Sauvignon Blanc was commercially produced in NZ for the first time in 1979 and is now NZ’s most widely planted variety. Sauvignon Blanc the explosive varietal that awoke the world to New Zealand wine.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Aotearoa is the Maori name for New Zealand'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' 100% New Zealand Sauvignon Blanc grapes'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: 'New Zealand'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' South Island-Marlborough'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '4008005630600'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: 11.5%vol
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 6.7 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' This Marlborough is fresh and zippy full of tropical and gooseberry fruit. Assails the senses with asparagus and gooseberry characters, lush tropical fruit overtones, fresh-cut grass, and grapefruit.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Perfect to sip on out in the sun. Enjoy with delicate fish (sole, shrimp, sushi) or a grilled chicken salad. The perfect summer wine.'
SKU:
  -
    product_variant: 750ml
    sku: '802338'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607367958
id: 14d9cabf-baa5-48c3-92c5-5002aca0f673
---
