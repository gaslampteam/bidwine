---
title: 'Kraut & Knolle - Premium Herbal Spirit'
product_producer: 91f470ee-0a15-4682-8a28-1d4a79197b26
product-image: product-images/kraut-knolle.jpg
product_sheet: downloads/Windspiel_Kraut_Knolle.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: Background
      -
        type: text
        marks:
          -
            type: bold
        text: ':'
      -
        type: text
        text: "\_ Exquisite Eifel potatoes meet the finest local herbs. Potatoes are ripened in volcanic soil, which are distilled in a special process. Dedicated to Frederick the Great, the discoverer of the potato in Germany and his other great passion: Greyhound dogs."
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Product: '
      -
        type: text
        text: 'Distilled from exquisite Eifel potatoes cultivated in volcanic soil.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Country of Origin:'
      -
        type: text
        marks:
          -
            type: bold
        text: "\_ "
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: "Technical data:\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_ "
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of bottle :'
      -
        type: text
        text: ' 500 ml bottle'
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_ "
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Closure:'
      -
        type: text
        text: ' cork closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: "\_ UPC"
      -
        type: text
        text: ': 426273133850'
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_"
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_ "
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content:'
      -
        type: text
        text: "\_ 36% vol.\_"
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Tasting Notes:'
      -
        type: text
        text: "\_ The unique composition of mild, slightly spicy potato alcohol with fresh, aromatic herbs from Eifel and sweet, varied and exotic ingredients results in a herbal spirit that simply ticks all the boxes. The varied interplay between nose and palette, between freshness, spiciness and light sweetness ensures pure enjoyment and makes this drink the perfect digestif for every occasion."
SKU:
  -
    product_variant: 500ml
    sku: '847832'
    link_override: null
    type: sku
    enabled: true
product_categories:
  - herbal-spirit
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1620424893
id: f27c6b2f-2e9c-4a70-a9b5-ce39d8339926
---
