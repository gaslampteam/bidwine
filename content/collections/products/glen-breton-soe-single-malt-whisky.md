---
title: 'Glen Breton SOE Single Malt Whisky'
product_producer: 71027d16-ef68-4f34-bbe8-c7a6a1e93564
product-image: product-images/SOE-Glen-Breton_Page_1_Image_0002.jpg
product_sheet: product-images/SOE-Glen-Breton.pdf
awards_bard_field:
  -
    type: set
    attrs:
      values:
        type: heading_set
        heading_text: 'SOE Salutes Our Veterans'
        margin_top: '0'
        margin_bottom: default
        heading_size: h2
        header_looks_like: h3
        heading-float: left
        heading_color: 0
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Special Operations Executive Commemorative Whisky. 2020 marks the 80th Anniversary of the Special Operations Executive (SOE).The legendary SOE was created on July 22,1940. It was a secret British WWII organization whose purpose was to conduct espionage , sabotage and reconnaissance in occupied Europe.'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Product:'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: bold
        text: 'North America’s FIRST Single Malt Whisky Distillery!'
      -
        type: text
        text: ' The crystal clear water from MacLellan’s Brook flows from the Mabou Highlands located above Glenora Distillery, giving Glen Breton its classic and pure taste. Our signature single malt product'
      -
        type: text
        marks:
          -
            type: bold
        text: ' Aged 10 years'
      -
        type: text
        text: ' with America Oak. Produced by the traditional copper pot stills methods using'
      -
        type: text
        marks:
          -
            type: bold
        text: ' only 3 ingredients'
      -
        type: text
        text: ' Barley, Yeast & Water.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Cape Breton, Nova Scotia'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle :'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '817411001645'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 43 %vol'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' On the nose it is medium-bodied and pleasantly fiery, offering a tight combination of butterscotch, heather, honey, with wood-infused undertones. Finishes long and smooth.'
SKU:
  -
    product_variant: 750ml
    sku: '842169'
    link_override: null
    type: sku
    enabled: true
product_categories:
  - whiskey
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607446803
id: 996f09e5-59e5-4d9f-9c7c-f8a8472a85e4
---
