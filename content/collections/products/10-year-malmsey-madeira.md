---
title: '10 Year Malmsey Madeira'
product_producer: e64924b1-2005-4fb2-b046-1a18a1a34733
product-image: 'product-images/H&-H-Malmsey-10-year.jpg'
product_sheet: 'product-images/H&-H-Malmsey-10-years.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Wine Spectator '
      -
        type: text
        text: '- 91 points '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Monde Selection'
      -
        type: text
        text: ' - Brussels - '
      -
        type: text
        marks:
          -
            type: bold
          -
            type: italic
        text: 'Gold Medal'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Madeira wine is a still, fortified, blended wine that is wonderfully concentrated and complex. The Portuguese island of Madeira is called the Island of the Immortals in reference to its wines, which can hold-and improve- their quality over centuries. Henriques & Henriques is one the oldest and most prestigious of the wineries in Madeira. H & H is now the largest independent producer and shipper of Madeira Wines and the only Madeira House who owns vineyards. Madeira is one of the world’s great fortified wines.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Malvasia grapes aged 10 years in oak cask'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Portugal
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Madeira'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle : '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 5601196010085'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content:'
      -
        type: text
        text: ' 20 %vol'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Residual Sugar:'
      -
        type: text
        text: ' 112 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Malmsey is a extremely rich and luscious wine with a fine bouquet. Raisiny-fruity, full bodied, fragrant, very rich and sweet. It is rich, pungent exotically-flavored wine with refreshing acidity.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Enjoy as a dessert wine or with your favorite desserts. A rich exotically-flavored dessert wine with refreshing acidity and a long finish.'
SKU:
  -
    product_variant: 750ml
    sku: '736031'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607447891
id: 271f15a0-41d2-4ba9-9d18-62c6128f7f8f
---
