---
title: 'Dreams Traminer'
product_producer: 6c539cdf-1a95-48b5-8664-010c26c74012
product-image: product-images/Binder1_Page_36_Image_0002.jpg
product_sheet: 'downloads/Dreams Traminer.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold medal - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Gilbert and Gaillard International Competition 2017.'
  -
    type: paragraph
    content:
      -
        type: text
        text: '(Gilbert and Gaillard is the Robert Parker of Europe)'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '--Heaven Can Wait--'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Moldova has a well-established wine industry. In 2014, Moldova was the 12th largest wine-producing country in the world. Grape growing and winemaking began 4000 years ago.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'For more than 375 years Einig-Zenzen has been dedicated to the wine trade. Quality assurance starts at Einig-Zenzen with the very first step of careful selection and control of winegrowers. A fascination for wine as a fruit of mother nature is the driving force to enjoy a surprising and fascinating future.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Traminer A.K.A. Gewurztraminer'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: 'Moldova – Eastern Europe'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: ordered_list
    attrs:
      order: 1
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '4008005043578'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '12.5 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '5.3 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'A dry white wine. Pale gold color with endearing aromas of rose, passion fruit, and lychee notes. The same highly expressive aromatics flow through to the palate which is balanced, fresh, and finely textured. A delicious wine.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Enjoy as an aperitif Or with Asian cuisine and fleshy, fatty game.'
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607368122
SKU:
  -
    product_variant: 750ml
    sku: '802335'
    link_override: null
    type: sku
    enabled: true
id: 95fd9b81-6cb5-4134-8721-a0809ae82a76
---
