---
title: 'Antre Okuzgozu-Bobazkere'
product_producer: 069c1e10-7c64-4ff9-918c-ac699839693b
product-image: product-images/Antre-Okuzgozu-Bobazkere_Page_1_Image_0001.jpg
product_sheet: 'downloads/Antre Okuzgozu-Bobazkere.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Silver:'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'International Wine Challenge 2015'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Silver:'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Decanter Wine Challenge 2015'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Everything starts with a dream. This is Aegean Turkey, the homeland of Mythology and Dionysus…'
  -
    type: paragraph
    content:
      -
        type: text
        text: '…the birthplace of Domaine Lucien Arkas which inherited the fertility, diversity, and extraordinary nature of the Aegean Region. Our wines owe their dignity and vision to their owner: Lucien Arkas.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'His vision of the wine is decidedly qualitative. He realized the dream of producing the best wines in Turkey.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Domaine Lucien Arkas'
      -
        type: text
        text: ' manages the first organic vineyard certified by Ecocert in Turkey. An exceptional terroir spread over 200 hectares and blessed by the gentle Aegean climate. Priority is given to manual harvesting at night in order to maintain the quality of the grapes. A perfect balance between tradition and modernism allows the wines to age in French and American oak barrels or stainless steel tanks.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' A blend of estate-bottled Organic traditional Turkish grapes- Okuzgozu-Bogazkere that is hand-harvested at night to ensure the fruity aroma of the grapes.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Turkey
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '8698819984165'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '14.5 % alc. /vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '1.50gram of sugar per liter'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Full-bodied with strong tannins, and flavors of mulberry and black pepper with a complex flavor profile.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food suggestion: '
      -
        type: text
        text: 'Recommended with lamb and charcuterie plates. Also enjoy with casseroles, kebabs, smoked foods, and grilled meats.'
SKU:
  -
    product_variant: 750ml
    sku: '812400'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605039101
id: a18d6c09-0eca-48c6-bb0e-010176fc6d20
---
