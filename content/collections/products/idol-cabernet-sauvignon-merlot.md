---
title: 'Idol Cabernet Sauvignon Merlot'
product_producer: 069c1e10-7c64-4ff9-918c-ac699839693b
product-image: product-images/Binder1_Page_50_Image_0002.jpg
product_sheet: 'downloads/Idol Cabernet Sauvignon.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '-Organic-'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Everything starts with a dream. This is Aegean Turkey, the homeland of Mythology and Dionysus…'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: '…the birthplace of Domaine Lucien Arkas which inherited the fertility, diversity, and extraordinary nature of the Aegean Region. Our wines owe their dignity and vision to their owner: Lucien Arkas.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'His vision of the wine is decidedly qualitative. He realized the dream of producing the best wines in Turkey.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Domaine Lucien Arkas- manages the first organic vineyard certified by Ecocert in Turkey. An exceptional terroir spread over 200 hectares and blessed by the gentle Aegean climate. Priority is given to manual harvesting at night in order to maintain the quality of the grapes. A perfect balance between tradition and modernism allows the wines to age in French and American oak barrels or stainless steel tanks.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' An Organic blend of Cabernet Sauvignon and Merlot.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Turkey'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC :'
              -
                type: text
                text: ' 86988199 83007'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content :'
              -
                type: text
                text: ' 13.5 % alc./vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 1.80g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A striking bright ruby red. An intense nose of crunchy and juicy red fruit, raspberry; smoked and spicy notes complete this fruit bouquet. The wine is velvety, with cool and pleasant tannins. Friendly and convivial wine.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food suggestion: '
      -
        type: text
        text: 'Enjoy with grilled rump steak, chicken liver salad, or tasty ham.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving:'
      -
        type: text
        text: ' Serve at 16-18C'
SKU:
  -
    product_variant: 750ml
    sku: '791293'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605286657
id: 2276c01f-5c22-4875-acc6-99534538f868
---
