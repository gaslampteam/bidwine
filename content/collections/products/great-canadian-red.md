---
id: 9302c817-c5f0-497a-ba29-f226fc395a71
published: false
blueprint: products
title: 'Great Canadian Red'
product_producer: 5558f663-9c89-4076-98ff-0cc9e182ecb3
product-image: product-images/Binder1_Page_73_Image_0002.jpg
product_sheet: 'downloads/St Hubertus Great Canadian red.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' For over 25 years the Gebert family have tended the original Hughes Vineyards, which are among the first Okanagan grape plantings in 1928. They produce 100% estate-grown wines that reflect the terroir of the North Okanagan Valley. St Hubertus believe in the principles of organic & sustainable farming-No Pesticide -No Herbicide -No Fungicide'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' 100% estate-grown blend of Chambourcin, Pinot Noir, and Marechal Foch grapes fermented in American and French Oak'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality: '
      -
        type: text
        text: 'VQA –Vintners’ Quality Alliance - Is a Regulatory & appellation system that guarantees high quality and authenticity of origin.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' British Columbia- North Okanagan Valley'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '625259024159'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 12.8 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 2.8 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Simply a great North Okanagan Valley red wine. Discover the true flavors of the North. This classic cool climate wine.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Perfect match with BC West Coast seafood, wild mountain mushrooms, or some juicy Alberta steak.'
SKU:
  -
    product_variant: 750ml
    sku: '342691'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605304222
---
