---
title: 'King Shaka Zulu Chenin Blanc'
product_producer: 21047df7-1bda-43ae-a54d-c1594901c112
product-image: product-images/Binder1_Page_53_Image_0002.jpg
product_sheet: 'downloads/King Shaka Zulu Chenin Blanc.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Mundus Vini International 2015 -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: Gold
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The salute “'
      -
        type: text
        marks:
          -
            type: italic
        text: Bayede!
      -
        type: text
        text: '” was shouted by King Shaka Zulu to unite the nations, and is still today used to greet His Majesty King Goodwill Zwelithini Kabhekuzulu. The Shaka Zulu brand was developed to unite the nations in an effort to create sustainable jobs. The beadwork around the neck of the bottle is made by women in rural areas. The Royal Zulu household is the beneficial endorser of this wine that represents the richness and variety in a Simple yet Elegant Style. Shaka Zulu is not just a wine, it is a reflection of 200 years of tradition and excellence.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Unwooded Chenin Blanc'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: 'South Africa: Wellington-Boland Winelands region'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 600980138905'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 14 %'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual sugar:'
              -
                type: text
                text: ' 6.90g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A wine fit for a King. Fresh, fruity, and soft-drinking wine with a tropical bouquet and golden delicious apple whiffs. A zesty wine with a refreshingly crisp finish. Floral aromas with apple and pear-like flavors and assertive acidity.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food suggestions:'
      -
        type: text
        text: ' Because of the wine’s awesome acidity and inherently sweet flavor, you’ll find it pairs well with foods that have a sweet and sour element. Ideal with Asian cuisine or pork chops with apples. Try with your next Thanksgiving Turkey dinner.'
SKU:
  -
    product_variant: 750ml
    sku: '781787'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607368210
id: f54a008c-7733-411a-9634-c2e2f3275926
---
