---
id: 6b07b906-7e9e-465d-b42b-2f303e7c09b4
blueprint: products
title: 'Pinot Noir'
product_producer: dab49389-1538-4cde-bdc1-1e6a2e4f40da
product-image: product-images/waipara_west_pinot_noir.jpg
product_sheet: downloads/waipara_west_Pinot-Noir.pdf
awards_bard_field:
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'International Wine & Spirits Competition 2020'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Silver Medal Decanter World Wine Awards 2020'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Silver Medal Robert Parker’s Wine Advocate-88 points'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' A family owned vineyard located some 40 miles from Christchurch, situated on north facing sun drenched terraces along the Waipara River. At Waipara West they do not use insecticides, residual herbicides or artificial fertilizer. Only hand -picked grapes from their own vineyards are used for their wines. Described as more European in style than New World.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' 100% hand-picked Pinot Noir grapes fermented with whole cluster. The wine is aged 10 months in new French oak.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: 'New Zealand'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Canterbury/Waipara
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 9418587630144'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 14 % vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 1 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A delicious, elegant Pinot Noir. Deep in color fragrant with roses, raspberries, cherries and sandelwood. Fresh, pure, polished fruit on the palate. Rounded and velvety with delicate, well integrated oak and a clean cranberry cherry finish.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' A great accompaniment to game or poultry. Also pairs well with strong cheeses, pastas and pork or a fine dish of duck. A very versatile and food-friendly wine.'
SKU:
  -
    product_variant: 750ml
    sku: '611343'
    type: sku
    enabled: true
product_categories:
  - pino-noir
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1626809124
---
