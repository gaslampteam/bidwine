---
id: afe6bca2-22f0-4a5d-a85a-d687ec6d5fcb
blueprint: products
title: 'Riesling QBA Feinherb'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/riesling_fienherb.jpg
product_sheet: downloads/Propstei-Ebernach-Riesling-Feinherb.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The brothers Dr Peter-Josef Zenzen and Heinz-Rudolf Zenzen have set themselves the goal of successfully continuing the long tradition of the Ebernach Monastery winery. The viticulture of the monastery goes back 350 years. Such a tradition needs experience-and the Zenzen brothers have it. The family is already in the 13th generation as winemaking.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Riesling QBA Off Dry'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin'
      -
        type: text
        text: ': Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Mosel'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 400 155 10 200 24'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content: 11.5%vol'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 21.1 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'A seductively delicate residual sweetness make this off-dry wine melt on the tongue. Due to its light mineral acidity, which typically reflects the character of the Mosel Riesling, a long-lasting taste experience is created.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' A Riesling for all occasions. Fresh and stimulating, yet easy to be enjoyed alone or to be the center of a sociable round. The ideal companion to tender, light meat dishes, light fish variations or to a crisp, healthy salad.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving: '
      -
        type: text
        text: 'Serve slightly chilled at 8-10C'
SKU:
  -
    product_variant: 750ml
    sku: '852973'
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1626815010
---
