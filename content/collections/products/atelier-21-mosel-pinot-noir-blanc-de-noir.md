---
id: 80055255-6400-4aab-acae-d24f3b47e191
blueprint: products
title: '“Atelier 21” Mosel Pinot Noir Blanc de Noir'
product_producer: dfe327fa-39ee-4671-bea2-f089ad9ae2ba
product-image: product-images/20000013_Pinot_Noir_Blanc_de_Noir_eng.jpg
product_sheet: downloads/Kloster-Ebernach-858238.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' In 1130 Franciscan Brothers of the Holy Cross acquired the Ebernach monastery and in 1887 began their work with people with disabilities. Ebernach monastery now accompanies people in need of assistance. We are enthusiastic about the work of the residents and artists and of this unique collaboration.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' '
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Pinot Noir Blanc-Estate Wine'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Country of Origin: Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Mosel
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 400 155 1020291'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 12.50%vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 23.1g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A white wine made from a dark-skinned grape the` Pinot Noir’. Creamy aromas of peach and passion fruit rounded off by fine nuances of oranges.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Pairs well with light meats like roast chicken, salad and pasta.'
SKU:
  -
    product_variant: 750ml
    sku: '858238'
    type: sku
    enabled: true
product_categories:
  - wine
  - white
  - pinot-noir
  - pinot-noir-blanc
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1638230341
---
