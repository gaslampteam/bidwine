---
id: 221fd6f5-1801-4f8e-899c-589fec23d0b8
blueprint: products
title: Riesling
product_producer: dab49389-1538-4cde-bdc1-1e6a2e4f40da
product-image: product-images/waipara_west_riesling.jpg
product_sheet: product-images/waipara_west_riesling.jpg
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'International Wine & Spirits Competition 2017-Silver Medal'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'A family owned vineyard located some 40 miles from Christchurch, situated on north facing sun drenched terraces along the Waipara River. At Waipara West they do not use insecticides, residual herbicides or artificial fertilizer. Only hand -picked grapes from their own vineyards are used for their wines. Described as more European in style than New World.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' 100% hand-picked Riesling grapes from two blocks with different characteristics, one low yielding giving concentrated citrus fruit, the other more vigorous and lush. The fruit is then whole bunch pressed to achieve a very gentle juice extraction,'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' New Zealand'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Canterbury/Waipara
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 9418587680002'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 12.2 % vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 4.0 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'A marvelously taut wine, almost spritzy fresh. Fabulous flavor of lemon zest, crisp apple, the smokiness of tar and finally an inviting soft center of honey, bread and cream. Refreshing dry with great acidity.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' The ideal wine for Asian cuisine. Also can be served with mild cheese, or poultry.'
SKU:
  -
    product_variant: 750ml
    sku: '611376'
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1626809271
---
