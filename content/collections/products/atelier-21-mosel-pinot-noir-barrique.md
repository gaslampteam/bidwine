---
id: 6a329379-5db9-4c3e-9140-493a9d84429a
blueprint: products
title: '“Atelier 21” Mosel Pinot Noir Barrique'
product_producer: dfe327fa-39ee-4671-bea2-f089ad9ae2ba
product-image: product-images/1900010_Pinot_Noir_Atelier21_eng.jpg
product_sheet: downloads/Kloster-Ebernach-852982.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' In 1130 Franciscan Brothers of the Holy Cross acquired the Ebernach monastery and in 1887 began their work with people with disabilities. Ebernach monastery now accompanies people in need of assistance. We are enthusiastic about the work of the residents and artists and of this unique collaboration.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' '
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Pinot Noir Barrique-Estate Wine'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Matured in oak barrels'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Mosel
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 400 155 1030139'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 13.0%vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 2.0.g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' The aromas of the Pinot Noir relates to cherries, graphite, cocoa, vanilla, juniper, caramel and licorice. Fruity elegant and silky structure with beautiful balanced properties round tannin structure with a mineral finish.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Pairs well with a wide range of foods but perfect with red meats-roasted or grilled and pasta. Stands well on its own.'
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1638230026
SKU:
  -
    product_variant: 750ml
    sku: '852982'
    type: sku
    enabled: true
product_categories:
  - pino-noir
  - wine
---
