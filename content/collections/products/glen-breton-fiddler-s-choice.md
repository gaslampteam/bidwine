---
title: 'Glen Breton Fiddler’s Choice'
product_producer: 71027d16-ef68-4f34-bbe8-c7a6a1e93564
product-image: product-images/Binder1_Page_41_Image_0002.jpg
product_sheet: 'downloads/Glenora Fiddlers Choice Box.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Distillery of the Year '
      -
        type: text
        text: '- '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Canada 2011'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'North America’s Only Single Malt Whisky Distillery'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'In the early 1800s, Scottish immigrants chose Cape Breton Island for their new home as its beauty so resembled the Highlands and Islands of Scotland. Many traditions and secrets came with these pioneers. The making of a spirited whisky was one of them. The crystal clear water from MacLellan’s Brook flows from the Mabou Highlands located above Glenora Distillery, giving Glen Breton its classic and pure taste. Fiddler’s Choice is a tribute to Cape Bretoner,- gifted fiddler, composter, and teacher-John MacDougall. Fiddler’s Choice celebrates fiddle music. The tradition of Scottish fiddle music continues to be preserved by Fiddler’s choice, the Spirit of Cape Breton.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Product:'
      -
        type: text
        text: ' Hand-crafted Single Malt Whisky produced in authentic copper pot stills. Matured for six years in Kentucky Bourbon barrels.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Canada
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Cape Breton, Nova Scotia'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '817411001799'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 43 % vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Tasting Notes:'
              -
                type: text
                text: ' A six-year whiskey with a light and smooth taste. Honey tones mixed with underlying apple and vanilla aromas transpose into a delightfully delicate and smooth yet refreshing taste experience.'
SKU:
  -
    product_variant: 750ml
    sku: '767594'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607369175
id: 8330c197-0f10-4633-8c7f-43f4011b5dd9
---
