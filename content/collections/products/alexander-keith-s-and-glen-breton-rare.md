---
id: 172d2d3a-954a-40b4-a0fb-a1b0320e7327
blueprint: products
title: 'Alexander Keith’s & Glen Breton Rare'
product_producer: 71027d16-ef68-4f34-bbe8-c7a6a1e93564
product-image: product-images/keiths-800.png
product_sheet: downloads/Alexander-Keith-s-bottle-box.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' North America’s FIRST Single Malt Whisky Distillery'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'In the early 1800’s, Scottish immigrants chose Cape Breton Island for their new home as its beauty so resembled the Highlands ad islands of Scotland. Many tradition and secrets came with these pioneers. The making of a spirited whisky was one of them. The crystal -clear water from MacLellan’s Brook flows from the Mabou Highlands located above Glenora Distillery, giving Glen Breton its classic and pure taste.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Product:'
      -
        type: text
        text: ' Many moons ago in the magical hills of Cape Breton, several batches of Alexander Keith’s IPA were distilled, placed in oak barrels and tucked in a dark corner of Glenora Distillery. After 18 years of quietly maturing, Magic!'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Canada
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Cape Breton, Nova Scotia'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle: 700 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: cork closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'UPC:'
      -
        type: text
        text: ' 0817411001829'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 43 %vol'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A wonderful taste sensation of malted barley that was enhanced by the spring waters that flow around our apple trees throughout our Cape Breton Highlands. Those who love will love it a lot. Maple cream notes of butterscotch and a light finish of apple.'
SKU:
  -
    product_variant: 700ml
    sku: '852927'
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1637019904
---
