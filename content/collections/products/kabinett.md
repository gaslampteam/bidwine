---
title: Kabinett
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/kabinett.jpg
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: '#1 Selling German White Wine in Western Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold Medal Winner'
      -
        type: text
        text: ' - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Gilbert and Gaillard International Competition 2017'
  -
    type: paragraph
    content:
      -
        type: text
        text: '(Gilbert & Gaillard is the Robert Parker of Europe)'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The house of Dr. Zenzen has since 1636 been wine growers in Germany. This family tradition of more than 380 years stands for great experience and exclusiveness concerning the quality of wines. The Einig- Zenzen family produces wine grown in the hills above the river Mosel in the small picturesque village of Valwig in the heart of the Mosel wine region.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Blend of selected white grapes-Riesling, Muller-Thurgau, and Kerner from the Rheinhessen growing area.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality:'
      -
        type: text
        text: ' QmP- Qualitatswein mit Pradikat-Kabinett'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Gau Odernheimer Peterberg/ Bereich Wonnegegau/ Rheinhessen'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The Rheinhessen is protected from cold winds and strong rainfall by surrounding hills, making the area good for wine growing.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical Data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml blue Noblesse bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: UPC
      -
        type: text
        text: ':'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '4008005149041 - 750ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '4008005030349 - 1.5L'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical Data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 9.0%vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 39g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Gluten:'
              -
                type: text
                text: ' Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' This Kabinett wine is a delightfully fruity and light wine, which displays wonderful green apple and honeysuckle scents on the nose. The palate is rich in flavors of green and red apples in an off-dry style. It is balanced with moderate acidity. '
      -
        type: text
        marks:
          -
            type: bold
          -
            type: italic
        text: 'The number #1 selling German white wine in Western Canada.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Our Kabinett goes very well with any light food as chicken, seafood, and pasta dishes. It is also delightful as a refreshing cocktail before meals and at parties enjoying good company.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving:'
      -
        type: text
        text: ' Serve slightly chilled at 10-12 C'
SKU:
  -
    product_variant: 750ml
    sku: '77693'
    link_override: null
    type: sku
    enabled: true
  -
    product_variant: 1.5L
    sku: '285635'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607366463
product_sheet: downloads/Dr-Zenzen-Kabinett.pdf
id: b388b1d2-097c-44ae-bffd-2807711e85e8
---
