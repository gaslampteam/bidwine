---
id: b23edb44-30e0-4d1d-ab07-58db6d642f87
blueprint: products
title: 'Two Terrace Pinot Noir'
product_producer: dab49389-1538-4cde-bdc1-1e6a2e4f40da
product-image: product-images/waipara_west_two_terrace_pinot_noir.jpg
product_sheet: downloads/waipara_west_Two-Terrace-Pinot-Noir.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'A family- owned viney ard located some 40 miles from Christchurch, situated on north facing sun drenched terraces along the Waipara River. At Waipara West they do not use insecticides, residual herbicides or artificial fertilizer. Only hand -picked grapes from their own vineyards are used for their wines. Described as more European in style than New World.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: '100% hand-picked Pinot Noir grapes. Then 12 months in barrel undergoing malolactic fermentation.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' New Zealand'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Canterbury/Waipara'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle: 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 9418587840185'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 13.5 % vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 5.63 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Bright aromatics of fresh cherries, raspberries, wood spices and tobacco. With time, subtle notes of forest undergrowth, floral perfumes and worn leather are revealed. Light fruit sweetness, fine silky tannin and restrained volume allow savory elements to spice evenly into the red berry fruit core and crunchy acidity.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' A great accompaniment to game or poultry. Also pairs well with strong cheeses, pastas and pork or a fine dish of duck. A very versatile and food-friendly wine.'
SKU:
  -
    product_variant: 750ml
    sku: '846306'
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1626809414
---
