---
title: 'Dreams-Pinot Noir Blush'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Dr-ZenzenDreams-Pinot-Noir-Blush.jpg
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Grapes have been grown in Rheinhessen since Roman times. It is the largest wine-growing region in Germany with 26,563 hectares of vineyards. Rheinhessen is a region at the north-west end. Since May 2008 Mainz and Rheinhessen are members of the Great Wine Capitals Global Network (GWC), a federation of the world’s best-known vineyards.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Pinot Noir grapes with skins in contact with the juice just long enough to get the lovely Rose color.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Rheinhessen'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The Rheinhessen is protected from cold winds and strong rainfall by surrounding hills, making the area good for wine growing.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical Data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 400 800 50 608 72'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical Data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: 12.5%vol
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 18.1g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Gluten: '
              -
                type: text
                text: 'Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Delicate and refreshing blush Pinot Noir with a rose-hued color. Aromas and flavors that burst with vibrant notes of ripe and red berries as well as a touch of fresh strawberries with a crisp hint of medium-sweet finish on the palate.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Enjoy this fruity rose with light meat or fish dishes as well as on its own.'
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607365580
SKU:
  -
    product_variant: 750ml
    sku: '834216'
    link_override: null
    type: sku
    enabled: true
product_sheet: downloads/Dr-ZenzenDreams-Pinot-Noir-Blush.pdf
id: 67c55e04-d58c-4ced-844e-b5e612cb829d
---
