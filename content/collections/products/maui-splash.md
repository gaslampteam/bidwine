---
title: 'Maui Splash'
product_producer: 36f7c238-1c91-4ecd-ac5f-600a3921d2a5
product-image: product-images/Binder1_Page_59_Image_0002.jpg
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Popular - Passionate - Fun'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'MauiWines formerly Tedeschi Winery is Hawaii''s oldest winery and is set on the slopes of Upcountry Maui, 2000 feet above sea level, amid the vast, twenty-thousand acre `Ulupalakua Ranch’. Twenty-two acres of gradually sloping volcanic grasslands are used to cultivate the grapes. Established in 1974. While waiting for the newly planted grapes to mature, our talented wine-makers tested their skills on a different juice- the fruit that defines Hawaii, the pineapple.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: '100% Maui-grown gold pineapple and passion fruit essence'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: 'United States'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Hawaii-Maui'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '3005200130'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '11.8 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '52.30 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Additives: '
              -
                type: text
                text: 'contains sulfites as a preservative'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Light and fun, this pineapple wine is sweet, yet finishes with nice acidity and fruity freshness. Bursting with tropical flavors, this wine is fruit-forward and fun. A splash of Hawaii in every glass.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Pairs perfectly with sunshine and white beaches. Sweet memories of Maui in a glass. Wonderful served as an aperitif or as a dessert wine.'
SKU:
  -
    product_variant: 750ml
    sku: '579680'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607369679
product_sheet: downloads/Maui-Splash.pdf
id: 803e5931-b4a9-4a6c-ba72-dd6b34979ffa
---
