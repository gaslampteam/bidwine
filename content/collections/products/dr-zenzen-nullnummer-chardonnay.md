---
id: 0019b577-3ffe-4aee-972d-92851cb8dbbd
blueprint: products
title: 'Nullnummer Chardonnay'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/non-alc-chardonnay.jpg
product_sheet: downloads/Nullnummer-Chardonnayl.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The house of Dr Zenzen have since 1636 been wine growers in Germany. This family tradition of more 380 years stands for great experience and exclusiveness concerning the quality of wines.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Produced from 100% chardonnay grapes'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin :'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Rheinpfalz'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of closure : Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC : 4008005045374'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 0.0% vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Full-bodied on the palate with notes of yellow apple, gooseberry, melon and banana, accompanied by a hint of vanilla. A NON-ALCOHOL WHITE all the flavor and finesse but none of the calories or alcohol.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Pairing:'
      -
        type: text
        text: ' This alcohol free Chardonnay is a wonderful accompaniment to spicy dishes.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving: '
      -
        type: text
        text: 'Best served chilled.'
SKU:
  -
    product_variant: 750ml
    sku: '852967'
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1627067577
product_categories:
  - non-alcoholic
---
