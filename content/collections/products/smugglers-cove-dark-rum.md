---
title: 'Smugglers Cove Dark Rum'
product_producer: 71027d16-ef68-4f34-bbe8-c7a6a1e93564
product-image: product-images/Binder1_Page_71_Image_0002.jpg
product_sheet: 'downloads/Smugglers Rum.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'International Rum Festival 2003 -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Gold Medal Winner'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' North America’s Only Single Malt Whisky Distillery'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'In the early 1800s, Scottish immigrants chose Cape Breton Island for their new home as its beauty so resembled the Highlands and Islands of Scotland. Many traditions and secrets came with these pioneers. The making of a spirited whisky was one of them. The crystal clear water from MacLellan’s Brook flows from the Mabou Highlands located above Glenora Distillery, giving Glen Breton its classic and pure taste.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Product:'
      -
        type: text
        text: ' Dark rum blended from Jamaican stocks which have been aged a minimum of 2 years. Just like smugglers using the cover of the night to deliver goods during prohibition, this rum’s rich and deep auburn darkness conceals within the history of its making.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: 'Cape Breton, Nova Scotia'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 627040065341'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 45 %vol'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' The aroma is clearly that of a pot still rum, with notes of molasses, nutmeg, and hints of vanilla. Opens softly with fruit and spice elements, ending slightly warm and dry.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Great in mixed drinks and hot beverages'
SKU:
  -
    product_variant: 750ml
    sku: '277343'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605302804
id: f7f26f43-2afc-4bff-8aef-d4a9396e7070
---
