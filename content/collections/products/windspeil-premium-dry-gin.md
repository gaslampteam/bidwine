---
title: 'Premium Dry Gin'
product_producer: 91f470ee-0a15-4682-8a28-1d4a79197b26
product-image: product-images/premium_dry_gin.jpg
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Background:'
      -
        type: text
        marks:
          -
            type: bold
        text: " \_"
      -
        type: text
        text: 'The potatoes that make Windspiel Dry Gin are so unique grown in the volcanic soil of Eifel in Germany. This gin is dedicated to Frederick the Great who brought the potato to Germany and his second passion-Windspiel (“the greyhound”) dogs.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Product:'
      -
        type: text
        marks:
          -
            type: bold
        text: ' '
      -
        type: text
        text: 'Distilled from potatoes cultivated in volcanic soil'
      -
        type: text
        marks:
          -
            type: bold
        text: .
      -
        type: text
        text: ' Combine'
      -
        type: text
        marks:
          -
            type: bold
        text: ' '
      -
        type: text
        text: 'traditional juniper berries with a floral lemon note to ensure the special note of the gin.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Country of Origin:'
      -
        type: text
        marks:
          -
            type: bold
        text: "\_ "
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: "Technical data:\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_ "
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of bottle :'
      -
        type: text
        text: ' 500 ml bottle'
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_ "
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Closure:'
      -
        type: text
        text: ' cork closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: "\_ UPC"
      -
        type: text
        text: ': 4260273133546'
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_ "
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content:'
      -
        type: text
        text: "\_ 47 %vol"
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_"
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Tasting Notes:'
      -
        type: text
        text: "\_ Soft and elegant with a woody undertone. The smell of juniper combined with a floral lemon note and a touch of cinnamon dominate."
SKU:
  -
    product_variant: 500ml
    sku: '847830'
    link_override: null
    type: sku
    enabled: true
product_categories:
  - gin
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1620512877
product_sheet: downloads/Premium_Dry_Gin.pdf
id: 8b80c917-d867-4f89-8c9e-92f7c1351940
---
