---
title: '3G Malbec Steak Wine'
product_producer: 6c539cdf-1a95-48b5-8664-010c26c74012
product-image: product-images/3G-Malbec-Wine_Page_1_Image_0002.jpg
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good wine'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good Food '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good Friends'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'Malbec is Argentina’s “national variety”. The grape was first introduced to the region in 1868. And is now the most widely planted red grape in Argentina. Dense and dark, with full-bodied richness.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'For more than 380 years Einig-Zenzen our bottler has been dedicated to the wine trade. Quality assurance starts at Einig-Zenzen with the very first step of careful selection and control of winegrowers.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: Malbec
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Argentina'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 400800505910'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content: '
      -
        type: text
        text: '13.5 %vol'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Residual Sugar: '
      -
        type: text
        text: '5.6 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Deep color and intense fruity flavors with a velvety texture. Juicy fruit notes with rich spicness.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' A wine made for Good Food and Good Friends. Pairs well with steak and BBQ. Try with red meats, grilled meats and pasta with red tomato sauce.'
      -
        type: text
        marks:
          -
            type: bold
        text: ' 4 YOU.'
SKU:
  -
    product_variant: 750ml
    sku: '807581'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607367133
product_sheet: downloads/3G-Malbec-Wine.pdf
id: cb6383e1-e764-4ee6-bf0c-19cdfbd9b6be
---
