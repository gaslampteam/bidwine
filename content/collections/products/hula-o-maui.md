---
title: 'Hula O Maui'
product_producer: 36f7c238-1c91-4ecd-ac5f-600a3921d2a5
product-image: product-images/Binder1_Page_49_Image_0002.jpg
product_sheet: 'downloads/Hula O Maui.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold Medal & 90pts -'
      -
        type: text
        marks:
          -
            type: italic
        text: ' Los Angeles International Wine Competition'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'MauiWines formerly Tedeschi Winery is Hawaii''s oldest winery and is set on the slopes of Upcountry Maui, 2000 feet above sea level, amid the vast, twenty-thousand acre `Ulupalakua Ranch’. Twenty-two acres of gradually sloping volcanic grasslands are used to cultivate the grapes. Established in 1974. While waiting for the newly planted grapes to mature, our talented wine-makers tested their skills on a different juice- the fruit that defines Hawaii, the pineapple.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' 100% Maui-grown hand-picked gold pineapple'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' United States'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Hawaii-Maui'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 30052000123'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '12 % vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 19.7 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'The crisp, dry yet lively freshness of the pineapple explode from the glass. '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Methode champenoise'
      -
        type: text
        text: ' - Bottle Fermented techniques are used to make this product. Flavors of pineapple and soft tropical fruit make this wine easy to drink and excite the palate.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'This sparkling is a great accompaniment to all festive occasions and events that would use champagne as a special treat. Great as an aperitif or with seafood.'
SKU:
  -
    product_variant: 750ml
    sku: '579664'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605285314
id: e274ab45-aeea-46eb-8ef8-8a617a7fd206
---
