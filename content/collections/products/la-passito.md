---
title: 'La Passito'
product_producer: 069c1e10-7c64-4ff9-918c-ac699839693b
product-image: product-images/Binder1_Page_55_Image_0001.jpg
product_sheet: 'downloads/La Passito.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '-Organic-'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'China Wine and Spirits Awards 2014'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Everything starts with a dream. This is Aegean Turkey, the homeland of Mythology and Dionysus…'
  -
    type: paragraph
    content:
      -
        type: text
        text: '…the birthplace of Domaine Lucien Arkas which inherited the fertility, diversity, and extraordinary nature of the Aegean Region. Our wines owe their dignity and vision to their owner: Lucien Arkas.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'His vision of the wine is decidedly qualitative. He realized the dream of producing the best wines in Turkey.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Domaine Lucien Arkas -'
      -
        type: text
        text: ' manages the first organic vineyard certified by Ecocert in Turkey. An exceptional terroir spread over 200 hectares and blessed by the gentle Aegean climate. Priority is given to manual harvesting at night in order to maintain the quality of the grapes. A perfect balance between tradition and modernism allows the wines to age in French and American oak barrels or stainless steel tanks.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' An estate-bottled Organic Muscat hand-harvested at night to ensure the fruity aroma of the grapes. Passito is a winemaking method in which the grapes are dried before fermentation. First, the grapes are dried in bunches. As they dry the flavor and sugar levels increase. Till they reach the optimum sugar-acid balance.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Turkey
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 500 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 8698819982741'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 15 % alc. /vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 47.00g/L'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A beautifully balanced sweet wine, high in acidity with dominant sweet nuances of honey and citrus.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food suggestion:'
      -
        type: text
        text: ' Recommended with dessert, dried fruits, and aged cheeses. Offers a delightful drinking experience.'
SKU:
  -
    product_variant: 500ml
    sku: '812401'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605293235
id: 3539b200-1211-49dd-8343-034ad44f19b3
---
