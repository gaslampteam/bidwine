---
title: 'Pinot Blanc'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Binder1_Page_25_Image_0001.jpg
product_sheet: 'downloads/Dr Zenzen Pinot Blanc.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The house of Dr. Zenzen has since 1636 been wine growers in Germany. This family tradition of more than 380 years stands for great experience and exclusiveness concerning the quality of wines. This wine was grown in Rheinhessen one of the most important wine-growing in Germany. The term Feinherb on a German label is a fairly recent development, since 2003- meaning half-dry.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Pinot Blanc a.k.a. Weissburgunder or White Burgundy'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality: '
      -
        type: text
        text: 'QbA- German White Quality Wine'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'County of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: 'Rheinhessen - A region protected from cold winds and strong rainfall by surrounding hills, making the area good for wine growing.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical Data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Bottle: '
              -
                type: text
                text: '750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '400800 5043530'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical Data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '11% vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '28.2 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Gluten: '
              -
                type: text
                text: Gluten-free
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Bright, fresh, and exceptionally fruity, this wine also has a lovely mineral element that comes from the slate soil of the region. Smooth and elegant bursting with floral aromas and fruit flavors. Brimming with ripe pear, peach, and nectarine flavors.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'A food-friendly wine great with lighter dishes. Best served chilled and drunk on its own or paired with simple dishes such as seafood or light-flavored meats'
SKU:
  -
    product_variant: 750ml
    sku: '802339'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605202125
id: 7a9b719d-6981-4a14-8e73-7fea0fba744b
---
