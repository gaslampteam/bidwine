---
title: 'ZD Chardonnay'
product_producer: 4ffba8cc-6cb9-4b25-9ee1-9afb68d98861
product-image: product-images/Binder1_Page_83_Image_0002.jpg
product_sheet: downloads/ZD-Chardonnay.pdf
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Wine Enthusiast -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: '2018-91 points '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'The Wine Advocate - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Robert Parker - 91 points'
      -
        type: text
        text: ' '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold Medal -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'San Diego International Wine Competition-2018'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' One of Napa’s Premium wineries. ZD Wine is family owned and operated by three generations of the de Leuze family, located in the heart of Napa Valley. Founded in 1969. The ZD wine brand highlights the initials of the founding partners’ last name, but it also symbolizes the term Zero Defects, an acronym for the quality control program. ZD winery is a three-time award winner of the prestigious Decanter Award at the International Wine Competition in London. “This winery continues to turn out some of the most delicious and flavorful Chardonnay in California…” Robert Parker'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' 100% Chardonnay hand-harvested at night and fermented in 100% American Oak barrels for 10 months'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' United States'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' California-Napa Valley'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 44956001018'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 13.5%vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 2.8 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Aromas of pineapple, lime zest, and pear mingle with hints of vanilla and orange blossom. The palate is rich and full-bodied with an exceptional balance between fruit, toasty oak, and lively acidity. The finish is amazingly long and lasting. Enjoy!'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Goes great with seafood fine rich fish such as turbot or grilled veal chops with mushrooms.'
SKU:
  -
    product_variant: 750ml
    sku: '268227'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607369827
id: 98c2ac3a-6da5-447e-a202-55aaa3e3f0e2
---
