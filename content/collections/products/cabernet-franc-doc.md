---
title: 'Cabernet Franc DOC'
product_producer: 5dbf81a6-9d61-483f-a936-1d30b0008484
product-image: product-images/Binder1_Page_69_Image_0002.jpg
product_sheet: 'downloads/Rocca Bernarda Cabernet Franc.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'International Rum Festival 2003 -'
      -
        type: text
        marks:
          -
            type: bold
          -
            type: italic
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Gold Medal Winner'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Rocca Bernada wine growers since 1559. The vineyards are terraced into the hillside descending from the villa. Behind them rise the Julian Alps, which protect the grapes from the cold winds. The production philosophy of the winery is clear: territory, native grapes, and wines with the strong imprint of Friuli. Described by Forbes Magazine as a precious commodity. Rocca Bernarda belongs to the Order of Malta. The order has a long tradition of humanitarian aid-nurturing and serving the poor and sick- which become reality through humanitarian projects and social assistance in 120 countries.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Hand-harvested Cabernet Franc grapes from the terraced vineyards surrounding the ancient castle.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Italian
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: 'Friuli Colli Orientali D.O.C.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 8009924000037'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 13.0 %vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 1.00 g/liter'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Its aromas are intense, balanced, and fruity with floral and herbaceous notes. On the palate, it reveals a great structure and a full-bodied, velvety flavor.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' A wine which is considered to be very food-friendly because of its savory herbaceousness.Pairs well with earthy dishes; wild game, lamb, grilled steak, or chops.'
SKU:
  -
    product_variant: 750ml
    sku: '812726'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605302391
id: 2c0dec11-7e76-442b-a7e3-dc5a8397ce70
---
