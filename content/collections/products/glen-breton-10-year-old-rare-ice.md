---
title: 'Glen Breton 10 Year Old Rare Ice'
product_producer: 71027d16-ef68-4f34-bbe8-c7a6a1e93564
product-image: product-images/Binder1_Page_40_Image_0002.jpg
product_sheet: 'downloads/Glenora 10year Ice 750ml.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: italic
        text: '-Innovator of the Year-'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold Winner - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'San Francisco World Spirits Competition 2008'
      -
        type: text
        text: ' '
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' North America’s Only Single Malt Whisky Distillery'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'In the early 1800s, Scottish immigrants chose Cape Breton Island for their new home as its beauty so resembled the Highlands and Islands of Scotland. Many traditions and secrets came with these pioneers. The making of a spirited whisky was one of them. The crystal clear water from MacLellan’s Brook flows from the Mabou Highlands located above Glenora Distillery, giving Glen Breton its classic and pure taste. The world’s first single malt whisky aged in ice wine barrels. This is a Nova Scotia triumph.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Product: '
      -
        type: text
        text: 'Canada’s only Single Malt 10-year-Old Whisky aged in ice wine barrels.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Canada
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Cape Breton, Nova Scotia'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 817411001287'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '43 % vol.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' The bouquet is very elegant with a range of subtle spices, orangey citrus fruits, and a hint of ripe banana. Remarkably complex flavors of citrus, dried fruits, and spice play on the palate. Finishes with lightly peaty and cedary perfume. The ice wine barrel-takes off all the normally fiery edges of a 10year-old-whisky. Just incredible, utterly unique, and jaw-dropping!'
SKU:
  -
    product_variant: 750ml
    sku: '809604'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605282815
id: fcee4459-c92c-41a0-846f-09012fda2378
---
