---
title: 'Zentini Rose Pamplemousse'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Binder1_Page_85_Image_0001.jpg
product_sheet: 'downloads/Zentini Rose Pamplemousee.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The house of Dr. Zenzen has since 1636 been wine growers in Germany. This family tradition of more the 380 years stands for great experience and exclusiveness concerning the quality of wines. The Einig-Zenzen family produces wine grown in the hills above the river Mosel in the small picturesque village of Valwig in the heart of the Mosel wine region.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Rose grape wine and natural Grapefruit flavor.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '400800 5042328'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '8 % alc./Vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 77.5g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Gluten:'
              -
                type: text
                text: ' Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A light and refreshing cocktail, based on rose wine and grapefruit. A ready to drink Wine cocktail from the Dr. Zenzen family.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Pairing: '
      -
        type: text
        text: 'Enjoy with good friends and good company.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving:'
      -
        type: text
        text: ' Serve well chilled'
SKU:
  -
    product_variant: 750ml
    sku: '766063'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605305268
id: 2f288201-0d49-492b-a237-b8661764f359
---
