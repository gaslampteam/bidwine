---
title: '3G Chardonnay Sushi Wine'
product-image: product-images/3G-Chardonnay-Wine_Page_1_Image_0001.jpg
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good wine '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good Food '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good Friends'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Chardonnay is the queen of white grapes. The varietal is California’s most widely planted wine grape. Made from grapes that have reached their full potential under the California Sun.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'For more than 380 years Einig-Zenzen our bottler has been dedicated to the wine trade. Quality assurance starts at Einig-Zenzen with the very first step of careful selection and control of winegrowers.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Chardonnay'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' USA'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' California'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '4008005050927'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '13 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 5 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A lovely aroma of fresh pear, peach, apple and lemon fruit enhanced by a rich, creamy tone. The wine’s smooth, round texture, and rich, creamy peach and apple flavors persist into a long, flavorful aftertaste.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' A wine made for Good Food and Good Friends. This Chardonnay is a perfect match with Sushi. Also enjoy with chicken, turkey or other white meats. '
      -
        type: text
        marks:
          -
            type: bold
        text: '4 YOU.'
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607366714
SKU:
  -
    product_variant: 750ml
    sku: '807579'
    link_override: null
    type: sku
    enabled: true
product_sheet: downloads/3G-Chardonnay-Wine.pdf
product_producer: 6c539cdf-1a95-48b5-8664-010c26c74012
id: ad67ee82-639d-47b9-8682-045bdf8f9d89
---
