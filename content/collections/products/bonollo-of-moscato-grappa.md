---
title: 'Bonollo Of Moscato Grappa'
product_producer: 8effceca-b041-4d00-9b72-4bca76e6cf08
product-image: product-images/Binder1_Page_01_Image_0001.jpg
product_sheet: 'downloads/Bonollo Moscato.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The distillery Bonollo Umberto, was founded in 1908 by Giuseppe Bonollo, four generations producing fine grappa. Leading producers of quality Grappa since 1908.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Distillation of Moscato DOC wine dregs. Made from the pomace aka the skins, seeds, and stems left over from winemaking. The pomace is the best part of the grape it has the most aroma and flavors. The flavor profile of Grappa depends on the grape varietal used, in this case, Moscato.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Italy
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of bottle:'
      -
        type: text
        text: ' 700 ml bottle'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Closure:'
      -
        type: text
        text: ' cork closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'UPC:'
      -
        type: text
        text: ' 8000672119101'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content: '
      -
        type: text
        text: '40 % vol.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Aromatically spicy characteristics and intensely fruity sensations are typical of the grapes from which this grappa is produced. Results in a skillful combination of elevated frankness, balance, and a final persistence that result in a lovely aqua vitae.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Grappa is a digestif. In Italy, they call grappa “healthy water” because, at the end of a rich meal, they love to sip it while relaxing and digest. You can drink it straight, but you can also use it in cocktails, cooking, or alongside an espresso. Try soaking figs in grappa for a few days and you have an instant after-dinner drink. Or try leveraging grappa’s digestif qualities and using it intermittently throughout the meal. Using the right grappa glass is key. A good grappa glass is generally tall and narrow with a slightly wider opening at the top, to allow for the aromas to be enjoyed.'
SKU:
  -
    product_variant: 700ml
    sku: '724107'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605039699
id: 044df551-0fe1-4b7b-ba94-6128a04affa2
---
