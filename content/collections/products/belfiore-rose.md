---
title: 'Belfiore Rose'
product_producer: 5dbf81a6-9d61-483f-a936-1d30b0008484
product-image: product-images/Binder1_Page_02_Image_0001.jpg
product_sheet: 'downloads/Castello di Magione Rose.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'Castello di Magione, is a magnificent estate built to shelter pilgrims travelling to Rome or Jerusalem, dates back to 1150A.D. At Castello di Magione they bring a modern focus to their ancient craft, with sustainable viticulture practices that reduce the negative impact on the environment. These include late defoliation and natural antitranspirants, which help mitigate negative effects of global warming.'
  -
    type: paragraph
    content:
      -
        type: text
        text: '900 Years of Tradition in a Glass-Wine Production. CM belongs to the Order of Malta. The order has a long tradition of humanitarian aid-nurturing and serving the poor and sick- which become reality through humanitarian projects and social assistance in 120 countries.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: '100% Gamay grapes. Rose happens when the skins of red grapes (Gamay) touch the wine for only a short time.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Italian
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Umbria I.G.T.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 8009924013129'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 11.5 %vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '3.20 g/liter'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A fruit-forward and complex rose, with delicate floral aromas.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Rose is the wine of summer. Enjoy this lovely dry Rose with light pasta, seafood and grilled fish. Perfect for hot weather drinking.'
SKU:
  -
    product_variant: 750ml
    sku: '812723'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605047145
id: caa42f64-1860-4267-aaa1-8af74132628c
---
