---
title: 'Idol Ugni Blanc Chardonnay'
product_producer: 069c1e10-7c64-4ff9-918c-ac699839693b
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '-Organic-'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'Everything starts with a dream. This is Aegean Turkey, the homeland of Mythology and Dionysus…'
  -
    type: paragraph
    content:
      -
        type: text
        text: '…the birthplace of Domaine Lucien Arkas which inherited the fertility, diversity, and extraordinary nature of the Aegean Region. Our wines owe their dignity and vision to their owner: Lucien Arkas.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'His vision of the wine is decidedly qualitative. He realized the dream of producing the best wines in Turkey.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Domaine Lucien Arkas- '
      -
        type: text
        text: 'manages the first organic vineyard certified by Ecocert in Turkey. An exceptional terroir spread over 200 hectares and blessed by the gentle Aegean climate. Priority is given to manual harvesting at night in order to maintain the quality of the grapes. A perfect balance between tradition and modernism allows the wines to age in French and American oak barrels or stainless steel tanks.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' An '
      -
        type: text
        marks:
          -
            type: underline
        text: Organic
      -
        type: text
        text: ' blend of Ugni Blanc and Chardonnay vinified and aged in stainless steel tanks.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Turkey'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC : '
              -
                type: text
                text: '86988199 83021'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content :'
              -
                type: text
                text: ' 13 % alc./vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '2.40 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Fresh, Friendly, and Fruity. A pale yellow with golden, bright, and glowing hues. An intense nose with beautiful spring flowers, notes of citrus and peaches. Food Suggestion: An intense moment of happiness with a lobster and citrus, a muslin of smoked salmon with dill, or a good crottin of goat’s cheese.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving:'
      -
        type: text
        text: ' Serve slightly chilled at 10-12 C -This wine is the way for you to share a moment of relaxation with friends.'
  -
    type: paragraph
SKU:
  -
    product_variant: 750ml
    sku: '791292'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605287244
product-image: product-images/Binder1_Page_51_Image_0002.jpg
product_sheet: 'downloads/Idol Chardonnay.pdf'
id: 44681956-ec84-4292-8064-9e74327e60cf
---
