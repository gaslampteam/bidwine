---
id: d6605f9f-cb47-4de4-8f22-f40629e8ad51
blueprint: products
title: Chardonnay
product_producer: dab49389-1538-4cde-bdc1-1e6a2e4f40da
product-image: product-images/waipara_west_chard.jpg
product_sheet: downloads/waipara_west_Chardonnay.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' A family- owned vineyard located some 40 miles from Christchurch, situated on north facing sun drenched terraces along the Waipara River. At Waipara West they do not use insecticides, residual herbicides or artificial fertilizer. Only hand -picked grapes from their own vineyards are used for their wines. Described as more European in style than New World.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' 100% hand-picked Chardonnay grapes from five different parcels followed by Malolactic fermentation and nine months in barrel.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' New Zealand'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Canterbury/Waipara'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle: 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 9418587130194'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 14.0 % vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 6.68 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Tasting Notes: Ripe stone fruits, charred citrus, flinty Gunsmoke with underlying apple blossoms and honeysuckle. Understated oak influence coupled with our typical salt crust finish. A wine full of character.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Food Suggestion: Serve with mild, buttery or creamy dishes. Enjoy with meaty fish such as halibut, or cod Or subtly flavored seasoned poultry and pork dishes.'
SKU:
  -
    product_variant: 750ml
    sku: '846305'
    type: sku
    enabled: true
product_categories:
  - chardonnay
  - wine
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1626809811
---
