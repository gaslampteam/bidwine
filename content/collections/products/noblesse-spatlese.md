---
title: 'Noblesse Spatlese'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Binder1_Page_34_Image_0002.jpg
product_sheet: 'downloads/Dr ZenzenSpatlese.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold medal -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Gilbert & Gaillard International Competition 2017'
      -
        type: text
        text: ' (Gilbert & Gaillard is the Robert Parker of Europe)'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The house of Dr. Zenzen has since 1636 been wine growers in Germany. This family tradition of more than 380 years stands for great experience and exclusiveness concerning the quality of wines. The Einig-Zenzen family produces wine originating from the vine-growing region of Rheinhessen- Germany’s largest region of viticulture.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Blend of Muller-Thurgau and Riesling grapes'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality: '
      -
        type: text
        text: 'QmP- Qualitatswein mit Pradikat-Spatlese'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: 'Rheinhessen - A region protected from cold winds and strong rainfall by surrounding hills, making the area good for wine growing.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml blue Noblesse bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 4008005040478'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '9.5 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '48 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Gluten: '
              -
                type: text
                text: 'Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'A delightful fruity and mellow wine. Delicately fragrant with mangoes, pineapple and honeyed apples. Tropical fruit on the palate, it shows just a hint of fruit acidity and is desirably fresh and with a distinct zesty profile on the finish.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Enjoy our Spatlese with any light food such as chicken, seafood and pasta. It is also delightful as a refreshing'
SKU:
  -
    product_variant: 750ml
    sku: '308007'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605203011
id: 8248112b-6522-4810-84e0-bc9904286f95
---
