---
title: 'Hochheimer Holle Riesling Kabinett'
product_producer: 29a411d1-742d-4482-b211-868098700f30
product-image: product-images/Binder1_Page_48_Image_0002.jpg
product_sheet: downloads/Hochheimer-Holle--707522.pdf
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'International Wine Challenge 2009 - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Gold '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Wine Spectator - '
      -
        type: text
        marks:
          -
            type: italic
        text: '91 points'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' A family-owned estate since 1780. Now in the 8th generation, Hochheim is a cornerstone of the Rheingau for the finest Riesling. The estate was honored with the “State Honorary Award” for the best Riesling collection in the Rheingau region, gained at winning gold medals at every annual competition in the Rheingau.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'An estate-grown Riesling'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality:'
      -
        type: text
        text: ' QmP- Kabinett'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Kabinett means late picking when the grapes have been fully ripened & the rich mineral aromas transferred from the terroir into the grapes.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Rheingau
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'The vineyard-Holle is one of the best and most famous vineyards in the Rheingau region, officially classified as Premier Cru. Because of the terroir. the mineral-rich, chalky soil and special micro-climate, the Hochheim Riesling has an intense elegant character with rich aromas and fresh but moderate acidity.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '464154224048'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 8.5 % vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: 38g/l
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A wonderful fruity Riesling in the traditional style in perfect harmony between delicate sweetness and elegant acidity; clean lively fruitiness, rich aromas firm mature apple, peach, and apricot; a full mineral substance with good density; crisp and fresh.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Refreshing and appealing enjoyment for many dishes from pates to fish and light meat with creamy sauce, delicate cheeses, and dessert, as well on its own, sitting by the fireplace or on the terrace on a warm summer night'
SKU:
  -
    product_variant: 750ml
    sku: '707522'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607368822
id: c55c0f35-f461-439c-97c9-893b7a21d408
---
