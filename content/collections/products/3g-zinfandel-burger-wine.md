---
title: '3G Zinfandel Burger Wine'
product_producer: 6c539cdf-1a95-48b5-8664-010c26c74012
product-image: product-images/3G-Zinfandel-Burger-Wine_Page_1_Image_0002.jpg
product_sheet: 'downloads/3G Zinfandel Burger Wine.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good wine'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good Food '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good Friends'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'California is the largest wine producer of any state in the USA. With a tradition going back to the early 19th century, California has some of the oldest continuing vineyards in the country.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'For more than 380 years Einig-Zenzen our bottler has been dedicated to the wine trade. Quality assurance starts at Einig-Zenzen with the very first step of careful selection and control of winegrowers.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Zinfandel'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'California’s original grape'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' USA'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' California'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '4008005050897'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '13.5 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '8.8 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Aromas of dark red fruit, such as blackberry, raspberry, cherry and plum. On the palate, the wine is rich but also tart and spicy. Yummy!!'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' A wine made for Good Food and Good Friends. Enjoy with Burgers and BBQ . '
      -
        type: text
        marks:
          -
            type: bold
        text: '4 YOU.'
SKU:
  -
    product_variant: 750ml
    sku: '802337'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607367925
id: 9925e13a-10e5-4cb7-b1c4-5d0dec95d3ff
---
