---
title: 'Glen Breton Rare Canadian Malt Whisky'
product_producer: 71027d16-ef68-4f34-bbe8-c7a6a1e93564
product_sheet: downloads/New-label-Glen-Bretondocx.pdf
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Voted Top 50 Spirits in the World by Wine Enthusiast '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Glen Breton Rare has been selected as one of 101 whiskies to try before you die in the book of the same name by Ian Buxton “101 Whiskies To Try Before You Die”'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold & 95 pts'
      -
        type: text
        text: ' - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'International Review of Spirits-Chicago'
products_bard_field:
  -
    type: set
    attrs:
      values:
        type: heading_set
        heading_text: 'Glen Breton 10 Rare is now sporting it''s NEW Look!'
        margin_top: '0'
        margin_bottom: '2'
        heading_size: h2
        header_looks_like: h3
        heading-float: left
        heading_color: '#9D0000'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' North America’s Only Single Malt Whisky Distillery'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'In the early 1800s, Scottish immigrants chose Cape Breton Island for their new home as its beauty so resembled the Highlands and Islands of Scotland. Many traditions and secrets came with these pioneers. The making of a spirited whisky was one of them. The crystal clear water from MacLellan’s Brook flows from the Mabou Highlands located above Glenora Distillery, giving Glen Breton its classic and pure taste.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Product:'
      -
        type: text
        text: ' Our signature single malt product Aged 10 years with America Oak. Produced by the traditional copper pot stills methods using only 3 ingredients barley, yeast & water.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Canada
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Cape Breton, Nova Scotia'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'Cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '817411001027'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '43 %vol'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' On the nose it is medium-bodied and pleasantly fiery, offering a tight combination of butterscotch, heather, honey, and ground, with wood-infused undertones. Finishes long and smooth.'
SKU:
  -
    product_variant: 750ml
    sku: '709757'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607446401
product-image: product-images/Glen-Breton-Rare-10.jpg
id: 6298a669-5bdf-4ef1-8b9d-0f0513bbc600
---
