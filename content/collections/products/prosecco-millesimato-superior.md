---
title: 'Prosecco Millesimato Superior'
product_producer: 5dbf81a6-9d61-483f-a936-1d30b0008484
product-image: product-images/Binder1_Page_60_Image_0002-1605300898.jpg
product_sheet: 'downloads/Millesimato DOCG.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Winegrowers since 1559. Villa Giustiniani is produced in the heart of the Asolo DOCG production area, on the spectacular Montello hills. The deep, stony, glacial era morainic subsoils or “Red Asolo, yield a mineral-rich, structured, and long-lived Prosecco. Vineyards enjoy southwestern exposure and are ideally situated on a hillside near the Treviso plain where it is crossed by the Piave River.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' 100 % Glera (Prosecco) grapes grown in the hills of Montello in Veneto Italy.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Italy'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC :'
              -
                type: text
                text: '80099240 14898'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content :'
              -
                type: text
                text: ' 11.5 % alc./vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual sugar:'
              -
                type: text
                text: ' 25 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting notes:'
      -
        type: text
        text: ' A beautiful vintage sparkling with fine bubbles persistent perlage and generous, velvety foam. On the nose, there is a hint of fresh citrus, white flower, ripe apple, and pear. The taste is soft yet vibrant with a well-balanced freshness.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Excellent accompaniment to light, dry pastries.'
SKU:
  -
    product_variant: 750ml
    sku: '796704'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605301112
id: aaff310d-326d-4f9c-8b1e-477e163de265
---
