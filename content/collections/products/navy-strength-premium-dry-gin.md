---
title: 'Navy Strength Premium Dry Gin'
product_producer: 91f470ee-0a15-4682-8a28-1d4a79197b26
product-image: product-images/navy.jpg
product_sheet: downloads/Navy_Strength.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Background:'
      -
        type: text
        marks:
          -
            type: bold
        text: " \_"
      -
        type: text
        text: 'The amalgamation of British and German history in one bottle. Once stored next to gunpowder on the ships of the colonial rulers. The entire composition is perfected with a high-proof, yet mild-flavored potato alcohol which forms an ideal base for the botanicals.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Product:'
      -
        type: text
        marks:
          -
            type: bold
        text: ' '
      -
        type: text
        text: 'Distilled from potatoes cultivated in volcanic soil'
      -
        type: text
        marks:
          -
            type: bold
        text: '. '
      -
        type: text
        text: Blends
      -
        type: text
        marks:
          -
            type: bold
        text: ' '
      -
        type: text
        text: 'juniper, lemon and coriander with a hint of cinnamon and turns it into a burst of fireworks for the senses.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Country of Origin:'
      -
        type: text
        marks:
          -
            type: bold
        text: "\_ "
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: "Technical data:\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_ "
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of bottle :'
      -
        type: text
        text: ' 500 ml bottle'
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_ "
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Closure:'
      -
        type: text
        text: ' cork closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: "\_ UPC"
      -
        type: text
        text: ": 4260273133973\_"
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_ "
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content:'
      -
        type: text
        text: "\_ 57 %vol"
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: underline
        text: 'Tasting Notes:'
      -
        type: text
        text: "\_ Intense, aromatic juniper notes meet fine, potato alcohol. This very special gin is rounded off by floral citrus notes, cinnamon, cardamon and lavender."
SKU:
  -
    product_variant: 500ml
    sku: '847831'
    link_override: null
    type: sku
    enabled: true
product_categories:
  - gin
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1620424307
id: 349fac10-0240-4753-9809-ba1b8ba02952
---
