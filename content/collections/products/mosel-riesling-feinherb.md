---
id: 2d9e31ba-0e08-4af8-b441-59d4eed948a8
blueprint: products
title: 'Mosel Riesling Feinherb'
product_producer: dfe327fa-39ee-4671-bea2-f089ad9ae2ba
product-image: product-images/Kloster_Ebernach_Riesling_feinherb.jpg
product_sheet: downloads/Kolster---852975-Mosel-Riesling-Feinherb.pdf
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Gold at the 24th Berlin Wine trophy'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The brothers Dr Peter-Josef Zenzen and Heinz-Rudolf Zenzen have set themselves the goal of successfully continuing the long tradition of the Ebernach Monastery winery. The viticulture of the monastery goes back 350 years. Such a tradition needs experience-and the Zenzen brothers have it. The family is already in the 13th generation as winemaking.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Riesling -QbA Feinherb-Estate Wine'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Mosel'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 400 155 010300078'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 10.0%vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 18..0.g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A finely tart Riesling, elegant and fresh with delicate fruit aromas. A delightful harmonious balance between acidity, off-dry fruitiness and mineral elegance our Mosel wines are perfect for healthy and easy drinking. A combination you can find nowhere else.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Enjoy with crunchy salads, white meat and shellfish, but is also a pleasure on its own.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving:'
      -
        type: text
        text: ' Serve slightly chilled at 10-12 C'
SKU:
  -
    product_variant: 750ml
    sku: '852975'
    type: sku
    enabled: true
product_categories:
  - riesling
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1627344377
---
