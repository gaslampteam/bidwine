---
title: Gewurztraminer
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Binder1_Page_15_Image_0001.jpg
product_sheet: 'downloads/Dr Zenzen Gewurztraminer.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The house of Dr. Zenzen has since 1636 been wine growers in Germany. This family tradition of more than 380 years stands for great experience and exclusiveness concerning the quality of wines. This wine was grown in Rheinhessen one of the most important wine-growing regions in Germany. Gewürztraminer is a strong spicy white wine grape; it has a rich bouquet sometimes like perfume.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: '100% Gewurztraminer grape'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality:'
      -
        type: text
        text: ' QmP- Qualitatswein mit Pradikat-Kabinett'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Rheinhessen - A region protected from cold winds and strong rainfalls by surrounding hills, making the area good for wine growing.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical Data :'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Bottle: '
      -
        type: text
        text: '750ml Noblesse bottle'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Closure:'
      -
        type: text
        text: ' Stelvin closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'UPC:'
      -
        type: text
        text: ' 400800 5149416'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical Data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content:'
      -
        type: text
        text: ' 9.0% vol.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Residual Sugar:'
      -
        type: text
        text: ' 16.5g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gluten: '
      -
        type: text
        text: Gluten-free
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes'
      -
        type: text
        text: ': Intensely perfumed with aromas of rose petals, lychee nuts, and super-ripe pineapples. This wine is full-bodied, rich, lots of different aromas but not heavy. Still, you can taste the finesse of the grape variety.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Goes well with spicy foods, Asian food, and salads. '
      -
        type: text
        marks:
          -
            type: italic
        text: 'A real winner!'
SKU:
  -
    product_variant: 750ml
    sku: '583062'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605046200
id: 11c7c546-8681-45de-b577-b50a3200e645
---
