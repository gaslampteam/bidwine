---
title: 'Chateau Gaillot - Cotes de Duras'
product_producer: 6c539cdf-1a95-48b5-8664-010c26c74012
product-image: product-images/Binder1_Page_04_Image_0002.jpg
product_sheet: 'downloads/Chateau Gaillot.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '- Organic -'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gold Medal Winner - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Concours de Bordeaux 2017'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The Cotes de Duras oceanic climate is identical to that of the Bordeaux region. Wine from the Duras has been famous since the time of the French Monarch, Francis I.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'An Organic wine made from grapes grown in accordance with principles of organic farming, which typically excludes the use of artificial chemical fertilizers, pesticides, fungicides, and herbicides.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Cabernet Sauvignon, Cabernet Franc, and Merlot'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality: '
      -
        type: text
        text: 'AOC-Appellation d’ Origin Control'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: 'SouthWest France'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: 'Cotes de Duras'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of bottle: '
      -
        type: text
        text: '750 ml bottle'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Closure:'
      -
        type: text
        text: ' cork closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'UPC: '
      -
        type: text
        text: '4008005043707'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content:'
      -
        type: text
        text: ' 13 %vol'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Residual Sugar: '
      -
        type: text
        text: '1.3 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'A smooth and easy-drinking red. Great balance of dark red fruit, tannins, acidity with good length. Fruity with a hint of smokiness. A classically-styled, juicy red for everyday drinking pleasure.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Enjoy with beef, veal, game, and cheese.'
SKU:
  -
    product_variant: 750ml
    sku: '802336'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607368000
id: f6bccc5d-cdcb-4a00-a47a-dc61a7d42cd6
---
