---
id: 170bd3ef-e3b0-419c-840e-f5c6cd8797df
published: false
blueprint: products
title: 'Oak Bay Gewurztraminer'
product_producer: 5558f663-9c89-4076-98ff-0cc9e182ecb3
product-image: product-images/Binder1_Page_62_Image_0002.jpg
product_sheet: 'downloads/Oak bay Gewurztraminer.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '90 points “Editor’s Choice" -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Wine Enthusiast'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' For over 25 years the Gebert family have tended the original Hughes Vineyards, which are among the first Okanagan grape plantings in 1928. They produce 100% estate-grown wines that reflect the terroir of the North Okanagan Valley. St Hubertus believe in the principles of organic & sustainable farming-No Pesticide -No Herbicide -No Fungicide'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' A classic Alsatian-style wine 100% Gewurztraminer grapes grown on our Oak Bay Vineyard fermented 25 days in stainless steel.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality:'
      -
        type: text
        text: ' VQA –Vintners’ Quality Alliance Is a Regulatory & appellation system that guarantees high quality and authenticity of origin.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' British Columbia- North Okanagan Valley'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 625259202144'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '13.5 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 9.9 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Lychee nut aroma gives way to the intense spicy/floral bouquet that remains in the glass even the wine is gone! On the palate, there are touches of exotic spices, field flowers, and crisp acidity to balance the small number of residual sugars.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Chill a bottle to sip on the patio on a hot summer day or have it as an appetizer. Delicious with Chicken Curry or Salmon.'
SKU:
  -
    product_variant: 750ml
    sku: '597229'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605300120
---
