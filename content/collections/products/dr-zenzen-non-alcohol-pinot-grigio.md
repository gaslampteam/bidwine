---
id: 232bd0e8-18ec-4547-81f1-84f3f21a138e
blueprint: products
title: 'Non-Alcohol Pinot Grigio'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/non-alc-Pino-Grigio.jpg
product_sheet: downloads/Dr-Zenzen-Non-AlcoholPinot-Grigiol.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The house of Dr Zenzen have since 1636 been wine growers in Germany. This family tradition of more 385 years stands for great experience and exclusiveness concerning the quality of wines.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Produced from the Pinot Grigio grape ( Grauburgunder).'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin : '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Rheinpfalz
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of closure : Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC : 4008005045008'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data :'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 0.0% vol.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Fresh, crisp and full of citrus and peach flavors-full enjoyment with no alcohol. A NON-ALCOHOL WHITE all the flavor and finesse but none of the calories or alcohol.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Pairing: '
      -
        type: text
        text: 'At virtually any occasion or any time without the alcohol impact. This wine goes well with chicken, seafood, spicy pasta and pizza.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving: '
      -
        type: text
        text: 'Best served chilled.'
SKU:
  -
    product_variant: 750ml
    sku: '853050'
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1627068600
product_categories:
  - non-alcoholic
---
