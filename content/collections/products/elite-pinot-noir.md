---
title: 'Elite Pinot Noir'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Binder1_Page_31_Image_0002.jpg
product_sheet: 'downloads/Dr ZenzenElite Pinot Noir.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The house of Dr. Zenzen has since 1636 been wine growers in Germany. This family tradition of more than 380 years stands for great experience and exclusiveness concerning the quality of wines.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Pinot Noir Germany’s finest and foremost red wine variety.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality: '
      -
        type: text
        text: 'QbA- Qualitatswein'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Germany
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: 'Rheinhessen - a region protected from cold winds and strong rainfall by surrounding hills, making the area good for wine growing.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of bottle: '
      -
        type: text
        text: '750 ml flute bottle'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Closure: '
      -
        type: text
        text: 'Stelvin closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'UPC: '
      -
        type: text
        text: '4008005040270'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content: '
      -
        type: text
        text: '12 %vol.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Residual Sugar: '
      -
        type: text
        text: '5.5 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gluten:'
      -
        type: text
        text: ' Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A fruit-driven, soft velvety Pinot Noir, with medium body. Aromas are reminiscent of blackcurrant and cherry. Wonderful finish. Velvety and light-bodied with deep berry flavors, hints of strawberries and black cherries to excite the taste buds.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Pairs well with a wide range of foods, but perfect with red meats -roasted or grilled -and pasta. Stands well on its own.'
SKU:
  -
    product_variant: 750ml
    sku: '724937'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605204215
id: 2922db6c-98c2-4a02-815b-2f9fa0cb3176
---
