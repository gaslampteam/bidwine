---
title: '3G Sauvignon Blanc Fish Wine'
product_producer: 6c539cdf-1a95-48b5-8664-010c26c74012
product-image: product-images/3G-Sauvignon-Blanc_Page_1_Image_0002.jpg
product_sheet: 'downloads/3G Sauvignon Blanc.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good wine '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good Food '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Good Friends'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' Chile is known for making some of the most popular Sauvignon Blanc in the world. Light, refreshing, citrusy, and enjoyable. Great value. Zesty is the keyword for Sauvignon Blanc.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'For more than 380 years Einig-Zenzen our bottler has been dedicated to the wine trade. Quality assurance starts at Einig-Zenzen with the very first step of careful selection and control of winegrowers.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Sauvignon Blanc'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Chile'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 4008005050903'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 13 % vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 1.5 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Excellent acidity gives this wine a crisp, clean palate. Full flavors of citrus and lychee. Initial aromas of honeysuckle and orange zest are supported by grapefruit and gooseberry.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'A wine made for Good Food and Good Friends. A wine to be sipped all summer long or paired with seafood, shellfish, or a variety of white meat dishes.'
      -
        type: text
        marks:
          -
            type: bold
        text: ' 4 YOU'
SKU:
  -
    product_variant: 750ml
    sku: '807580'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607367902
id: cc947ea4-09b1-40e1-ae51-16e0255cc9ee
---
