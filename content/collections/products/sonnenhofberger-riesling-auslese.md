---
id: 5a7a563f-749a-4865-ada8-d4f817ddf429
blueprint: products
title: 'Sonnenhofberger Riesling Auslese'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Sonnenhofberger-Auslese-Bottle-picture.jpg
product_sheet: downloads/Dr-Zenzen--Riesling-Auslese-Sonnenhofberger.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The Sonnenhofberger is one of the hallmarks of the Dr Zenzen family Wine Estate. Wines under that name will be only released in exceptional years with special quality results from their vineyard in the Mosel river valley.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Produced from Riesling grapes -the Queen of German grape varietals.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality : '
      -
        type: text
        text: 'QmP- Qualitatswein mit Pradikat- Auslese'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Mosel-Sonnenhofberger'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml flute bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 400 500 5092019'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 10.0 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 45.0 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Produced the traditional way at least 2 years of ageing, yet surprisingly youthful, showing not just the honeyed apricot tones that can be expected from a mature Auslese, but also still maintaining apple and pear tones more common to younger wines of this type. This magnificent Riesling is rich in apple and pear, but not as sweet as many Auslese,'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Perfect with white meat, Asian or Indian cuisine. Also a perfect match with your favorite dessert or on its own.'
SKU:
  -
    product_variant: 750ml
    sku: '858237'
    type: sku
    enabled: true
product_categories:
  - wine
  - white
  - reisling
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1638231692
---
