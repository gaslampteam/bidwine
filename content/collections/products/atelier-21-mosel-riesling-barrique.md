---
id: af8b9cff-4391-4125-95ae-6001c188b1b3
blueprint: products
title: '“Atelier 21” Mosel Riesling Barrique'
product_producer: dfe327fa-39ee-4671-bea2-f089ad9ae2ba
product-image: product-images/2000005_Riesling_Barrique_Atelier21_eng.jpg
product_sheet: downloads/Kloster-Ebernach-858239.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' In 1130 Franciscan Brothers of the Holy Cross acquired the Ebernach monastery and in 1887 began their work with people with disabilities. Ebernach monastery now accompanies people in need of assistance. We are enthusiastic about the work of the residents and artists and of this unique collaboration.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Riesling Barrique-Estate Wine'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Matured in oak barrels'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Mosel
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 400 155 1020307'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 12.50%vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 11.30g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Typical barrique nose, delicate limes round off the taste with integrated nuances of vanilla. Reflecting the character of the Mosel Riesling.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' A Riesling for all occasions. Fresh and stimulating, yet easy to be enjoyed alone or to be the center of a sociable round. Enjoy with light meat dishes, fish or a crisp salad.'
product_categories:
  - reisling
  - red
  - wine
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1638230547
SKU:
  -
    product_variant: 750ml
    sku: '858239'
    type: sku
    enabled: true
---
