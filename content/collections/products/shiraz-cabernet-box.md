---
title: 'Shiraz/Cabernet - Box'
product_producer: 0559ace2-dfc9-465c-ba08-c71765ef2350
product-image: product-images/Binder1_Page_09_Image_0002.jpg
product_sheet: 'downloads/Crocodile Creek Shiraz Cab 3 Liter.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' A fruit-forward concentration of two great red wine varieties, known colloquially as “the Great Australian Red Blend”'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' A blend of the finest premium red grapes-50% Shiraz and 45% Cabernet Sauvignon and 5% other.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Australia
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 3 Liter bag-in-box'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 4022229301689'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '14 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 2.80 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Gluten:'
              -
                type: text
                text: ' Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Vibrant fruit, aromas of ripe plum, and cassis. A dry red that is full-bodied and spicy with aromas of ripe plum and cassis. Juicy, jammy fruit flavors in mid-palate.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Serve with roasted meat, steak, and cheese.'
SKU:
  -
    product_variant: '3L Box'
    sku: '818673'
    link_override: null
    type: sku
    enabled: true
product_categories:
  - wine
  - red
  - cabernet
  - shiraz
  - box
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605146591
id: f1ef0481-7026-4526-a75a-e3c8479fc1a9
---
