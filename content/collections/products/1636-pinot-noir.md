---
title: '1636 Pinot Noir'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Pinot_Noir_Page_1.jpg
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The house of Dr Zenzen have since 1636 been wine growers in Germany. This family tradition of more than 380 years stands for great experience and exclusiveness concerning the quality of wines. The Einig-Zenzen family produces wine grown in the hills above the river Mosel in the small picturesque village of Valwig in the heart of the Mosel wine region.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '1636-Our Heritage Line'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The 1636 label was commemorated for this important year in the Zenzen family. The date the family started in the wine business. Thirteen generations of Zenzen’s have been producing wine since 1636.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' Pinot Noir grape. The phrase Holy Grail is often associated with the Pinot Noir grape.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality:'
      -
        type: text
        text: ' QbA- Qualitatswein'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 4008005149898'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 14.5 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 3.9 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Gluten:'
              -
                type: text
                text: ' Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' The bouquet of extraordinary flavors can suggest anything from violets to truffles. On the palate you have a hint of raspberries and strawberries at the beginning leading to a jammy finish.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Great companion wine with any dish….experience the harmony with a lightly seared tuna dish or your favorite duck/goose creation.'
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607365536
SKU:
  -
    product_variant: 750ml
    sku: '759953'
    link_override: null
    type: sku
    enabled: true
product_sheet: downloads/Dr-Zenzen-1636-Pinot-Noir.pdf
id: 169d683d-2d82-4f9e-a3a0-f422959750a9
---
