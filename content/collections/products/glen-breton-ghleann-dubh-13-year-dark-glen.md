---
title: 'Glen Breton Ghleann Dubh 13 year Dark Glen'
product_producer: 71027d16-ef68-4f34-bbe8-c7a6a1e93564
product-image: product-images/Binder1_Page_42_Image_0002.jpg
product_sheet: 'downloads/Glenora Ghleann.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Silver Outstanding'
      -
        type: text
        text: ' - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'Whiskies of the World Awards 2017'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' North America’s Only Single Malt Whisky Distillery'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'In the early 1800s, Scottish immigrants chose Cape Breton Island for their new home as its beauty so resembled the Highlands and Islands of Scotland. Many traditions and secrets came with these pioneers. The making of a spirited whisky was one of them. The crystal clear water from MacLellan’s Brook flows from the Mabou Highlands located above Glenora Distillery, giving Glen Breton its classic and pure taste.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Product:'
      -
        type: text
        text: ' Hand-crafted Single Malt Whisky produced in authentic copper pot stills. Matured for 13 years in Kentucky Bourbon barrels.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: 'Cape Breton, Nova Scotia'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure: '
              -
                type: text
                text: 'cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '8017411001447'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 43 % vol.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A peaty whisky. Balance of smoke, peat, an apple that is agreeable to a novice drinker and very complex to please the connoisseur. On the palate has hints of smoke, malt sweetness, fruit, and nuttiness. Finishes with bursts of flavor development, apple, toffee, vanilla, and butterscotch. The Dark Glen!'
SKU:
  -
    product_variant: 750ml
    sku: '801826'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605208238
id: 81f87e3d-8d8c-4db1-8b27-c9b7c5924e91
---
