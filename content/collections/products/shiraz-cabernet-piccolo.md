---
id: c8cc1a16-c12f-4c8c-8e73-f8516f767ee9
blueprint: products
title: 'Shiraz/Cabernet - Piccolo'
product_producer: 0559ace2-dfc9-465c-ba08-c71765ef2350
product-image: product-images/Binder1_Page_10_Image_0002.jpg
product_sheet: downloads/Crocodile-Creek-Shiraz-Cab-250-ml.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' A fruit-forward concentration of two great red wine varieties, known colloquially as “the Great Australian Red Blend”'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' A blend of the finest premium red grapes-50% Shiraz and 45% Cabernet Sauvignon and 5% other.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Australia
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 250ml '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' Stelvin Closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 4022229302846'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '14 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 2.80 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Gluten:'
              -
                type: text
                text: ' Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'Vibrant fruit, aromas of ripe plum, and cassis. A dry red that is full-bodied and spicy with aromas of ripe plum and cassis. Juicy, jammy fruit flavors in mid-palate.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Serve with roasted meat, steak, and cheese.'
SKU:
  -
    product_variant: 250ml
    sku: '858683'
    type: sku
    enabled: true
product_categories:
  - wine
  - red
  - cabernet
  - shiraz
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1638231833
---
