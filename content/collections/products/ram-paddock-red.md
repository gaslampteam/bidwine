---
id: 5fafe9bc-c616-4b94-9ac0-8644f8e08c77
blueprint: products
title: 'Ram Paddock Red'
product_producer: dab49389-1538-4cde-bdc1-1e6a2e4f40da
product-image: product-images/waipara_west_paddock_red.jpg
product_sheet: downloads/waipara_west_Paddock-Red.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' A family owned vineyard located some 40 miles from Christchurch, situated on north facing sun drenched terraces along the Waipara River. At Waipara West they do not use insecticides, residual herbicides or artificial fertilizer. Only hand -picked grapes from their own vineyards are used for their wines. Described as more European in style than New World. Ram Paddock Red is our flagship red.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'A blend of Cabernet Franc 40%, Merlot 31% and Cabernet Sauvignon 29%. The grapes are hand harvested and fermented separately.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' New Zealand'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Canterbury/Waipara
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle: 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 9418587660981'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 15.0 % vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 5.4 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Tasting Notes: A cool climate Classic! Lovely cassis and spice notes from the Cabernet are filled out with plum and mocha notes from the Merlot and a flourish of perfume from the Cabernet Franc. In the mouth-silky and stylish with sweet plummy currant. It’s sex in a bottle. A wine that will reward cellaring. Waipara is one of the undiscovered gems of New Zealand’s South Island.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Enjoy with venison, strong cheeses and pasta dishes. Ideal with grilled fillet steak.'
SKU:
  -
    product_variant: 750ml
    sku: '611392'
    type: sku
    enabled: true
product_categories:
  - wine
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1626809448
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'International Wine & Spirits Competition 2017-Silver Medal'
---
