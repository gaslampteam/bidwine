---
title: 'Terrantez 20 year Madeira'
product_producer: e64924b1-2005-4fb2-b046-1a18a1a34733
product-image: product-images/Binder1_Page_45_Image_0002.jpg
product_sheet: 'downloads/H& H Terrantez 20 year.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '- Gold - London - '
      -
        type: text
        marks:
          -
            type: italic
        text: 'International Wine Challenge'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'Madeira wine is a still, fortified, blended wine that is wonderfully concentrated and complex. The Portuguese island of Madeira is called the Island of the Immortals in reference to its wines, which can hold-and improve- their quality over centuries. Henriques & Henriques is one the oldest and most prestigious of the wineries in Madeira. H & H is now the largest independent producer and shipper of Madeira Wines and the only Madeira House that owns vineyards. Madeira is one of the world’s great fortified wines.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Terrantez grapes aged 20 years in oak cask'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'The grape Terrantez is a near-extinct varietal, only 11 hectares remain in the world.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Portugal
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation: '
      -
        type: text
        text: Madeira
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 5601196010610'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 20 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar: '
              -
                type: text
                text: '74 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'A brilliant, old gold color with green nuances. Aromas of dried fruits namely raisin, hazelnuts and walnuts, and some wood in very good balance. Complex, medium-dry palate of bitter almond, dried fruits, spices, pepper, and some wood. A lasting aftertaste with the slight tang typical of this varietal.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Enjoy as a dessert wine or with your favorite desserts ie creme brulee. A rich exotically-flavored dessert wine with refreshing acidity and a long finish.'
SKU:
  -
    product_variant: 750ml
    sku: '743915'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605284611
id: 92a81fd2-fd7c-4468-9555-30bdc3757a14
---
