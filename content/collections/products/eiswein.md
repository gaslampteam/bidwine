---
id: 199cd154-8eec-4076-8c63-30e022445d2e
published: false
blueprint: products
title: Eiswein
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/Binder1_Page_17_Image_0001.jpg
product_sheet: 'downloads/Dr Zenzen Icewine.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The house of Dr. Zenzen has since 1636 been wine growers in Germany. This family tradition of more than 380 years stands for great experience and exclusiveness concerning the quality of wines.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal:'
      -
        type: text
        text: ' A wine made from frozen grapes produced from Weissburger (Pinot Blanc) and Silvaner grapes of Beerenauslese ripeness. Harvested and pressed while frozen. Icewine requires a hard freeze which means the grapes typically hang on the vine well beyond the normal harvest. Since the fruit must be pressed while still frozen, pickers often work at night or very early in the morning.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality:'
      -
        type: text
        text: ' QmP- Qualitatswein mit Pradikat -Eiswein'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Rheinhessen - A sweet white wine produced from selected grapes grown on the sunny hillsides of Rheinhessen in the Rhine valley.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Technical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of bottle'
      -
        type: text
        text: ': 375 ml bottle'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Closure:'
      -
        type: text
        text: ' Cork Closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'UPC:'
      -
        type: text
        text: ' 400800504553'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content:'
      -
        type: text
        text: ' 8 %vol'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Residual Sugar:'
      -
        type: text
        text: ' 70 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Gluten: '
      -
        type: text
        text: 'Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A truly unique wine with a remarkable concentration of fruity acidity and sweetness. Intense, decadent, and rich. In the mouth, the honeyed richness goes on and on. This wine has an intensely rich flavor, long-lasting mouthfeel with rich fruit aromas of peach and apricot.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' Enjoy this full-bodied elegant wine with a dessert such as fruit pies, cheesecake or sharp/rich cheeses, goose liver pate.Or delightful on its own.'
SKU:
  -
    product_variant: 375ml
    sku: '733635'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605046284
---
