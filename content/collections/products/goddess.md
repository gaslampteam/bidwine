---
id: c7df552f-6647-4ce8-97de-cb1502e9b21b
published: false
blueprint: products
title: Goddess
product_producer: 5558f663-9c89-4076-98ff-0cc9e182ecb3
product-image: product-images/Binder1_Page_77_Image_0002.jpg
product_sheet: 'downloads/St HubertusGoddess.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'For over 25 years the Gebert family have tended the original Hughes Vineyards, which are among the first Okanagan grape plantings in 1928. They produce 100% estate-grown wines that reflect the terroir of the North Okanagan Valley. St Hubertus believe in the principles of organic & sustainable farming-No Pesticide -No Herbicide -No Fungicide'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'A blend of Gewurztraminer, Pinot Blanc and Riesling'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quality: '
      -
        type: text
        text: 'VQA –Vintners’ Quality Alliance Is a Regulatory & appellation system that guarantees high quality and authenticity of origin.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Canada'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' British Columbia-North Okanagan Valley'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of bottle: '
      -
        type: text
        text: '750 ml bottle'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Type of Closure: '
      -
        type: text
        text: 'Stelvin closure'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'UPC:'
      -
        type: text
        text: ' 625259340662'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alcohol content: '
      -
        type: text
        text: '12.5 %vol.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Residual Sugar: '
      -
        type: text
        text: '12.1 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes: '
      -
        type: text
        text: 'A lush, fresh wine, just like you! It’s a highly aromatic wine- exotic floral notes and plenty of concentration. Made by women for women. The perfect ME wine. Food Suggestion: Reward yourself after all of life’s stresses and little emergencies. Will be a big hit when you have your girlfriends over for apps. Or grab a blanket and your favorite book and snuggle up to your inner Goddess with a glass.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'CSPC # '
SKU:
  -
    product_variant: 750ml
    sku: '749959'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605303096
---
