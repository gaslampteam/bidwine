---
id: 5f688c60-ec42-4992-8291-730af37ef374
blueprint: products
title: 'Riesling QBA Trocken'
product_producer: 24e0d905-5046-432d-ad2e-483883b70204
product-image: product-images/riesling_trocken.jpg
product_sheet: product-images/Propstei-Ebernach-Riesling-QBA-Trocken2.pdf
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'The brothers Dr Peter-Josef Zenzen and Heinz-Rudolf Zenzen have set themselves the goal of successfully continuing the long tradition of the Ebernach Monastery winery. The viticulture of the monastery goes back 350 years. Such a tradition needs experience-and the Zenzen brothers have it. The family is already in the 13th generation as winemaking.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Riesling QBA Trocken'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' Germany'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' Mosel'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of bottle : 750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Type of Closure: Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'UPC: 400 155 10 200 17'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alcohol content: 12.0%vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Residual Sugar: 6.4 g/l'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Gluten: Gluten Free'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' The nose is characterized by vegetative aromas, full of power and expressiveness, with a charming spice and wonderfully harmonious. An ideal wine for every day, both as a solo actor and as a food companion.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion: '
      -
        type: text
        text: 'Enjoy this Riesling with all cheese, pasta and fish dishes. We particularly recommend the wine with roasted veal medallions with glazed apples wedges and celery puree, or fillet of perch.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Serving:'
      -
        type: text
        text: ' Serve slightly chilled at 8-10 C'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'CSPC # 852971'
SKU:
  -
    product_variant: 750ml
    sku: '852971'
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1626819045
---
