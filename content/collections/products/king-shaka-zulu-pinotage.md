---
title: 'King Shaka Zulu Pinotage'
product_producer: 21047df7-1bda-43ae-a54d-c1594901c112
product-image: product-images/Binder1_Page_54_Image_0002.jpg
product_sheet: 'downloads/King Shaka Zulu Pinotage.pdf'
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background:'
      -
        type: text
        text: ' The salute “'
      -
        type: text
        marks:
          -
            type: italic
        text: Bayede!
      -
        type: text
        text: '” was shouted by King Shaka Zulu to unite the nations, and is still today used to greet His Majesty King Goodwill Zwelithini Kabhekuzulu. The Shaka Zulu brand was developed to unite the nations in an effort to create sustainable jobs. The beadwork around the neck of the bottle is made by women in rural areas. The Royal Zulu household is the beneficial endorser of this wine that represents the richness and variety in a Simple yet Elegant Style. Shaka Zulu is not just a wine, it is a reflection of 200 years of tradition and excellence. '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: 'Pinotage. The Pinotage grape was developed in 1925 by a South African viticulturist who crossed Pinot Noir and Cinsault (Hemitage). Pinotage has now become the signature South Africa grape.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin:'
      -
        type: text
        text: ' South Africa: Wellington-Boland Winelands region'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle:'
              -
                type: text
                text: ' 750 ml'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of closure: '
              -
                type: text
                text: 'Stelvin closure'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC:'
              -
                type: text
                text: ' 6009801318912'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content: '
              -
                type: text
                text: '14 %'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual sugar:'
              -
                type: text
                text: ' 2.90g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' A wine fit for a King. An elegant, deep red wine with plum and mulberry tones. This well-balanced wine encompasses red berry aromas with ripe banana combined with hints of delicious spicy vanilla, leading to a most pleasant finish.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food suggestions: '
      -
        type: text
        text: 'Enjoy with venison, spare ribs with a BBQ sauce, or osso buco. Try with pork risotto or chocolate truffle cake. But sometimes the best match is just you and a glass of Pinotage.'
SKU:
  -
    product_variant: 750ml
    sku: '781788'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1607368232
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Mundus Vini International 2014 -'
      -
        type: text
        text: ' '
      -
        type: text
        marks:
          -
            type: italic
        text: Silver
id: 1daafd33-ab17-4b6a-b3e9-526740f2c288
---
