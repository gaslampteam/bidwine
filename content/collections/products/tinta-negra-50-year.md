---
title: 'Tinta Negra 50 Year'
product_producer: e64924b1-2005-4fb2-b046-1a18a1a34733
product-image: product-images/Binder1_Page_46_Image_0002.jpg
product_sheet: 'downloads/H& H Tinta Negra 50year.pdf'
awards_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'International Wine Challenge '
      -
        type: text
        text: '-'
      -
        type: text
        marks:
          -
            type: italic
        text: ' Gold'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'BI Wine & Spirits - '
      -
        type: text
        marks:
          -
            type: italic
        text: '92 points'
  -
    type: paragraph
    content:
      -
        type: text
        text: '- '
      -
        type: text
        marks:
          -
            type: bold
        text: 'Best Fortified Wine of the Year 2017 - '
  -
    type: paragraph
products_bard_field:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Background: '
      -
        type: text
        text: 'Madeira wine is a still fortified, blended wine that is wonderfully concentrated and complex. The Portuguese island of Madeira is called the Island of the Immortals in reference to its wines, which can hold-and improve- their quality over centuries. Henriques & Henriques is one the oldest and most prestigious of the wineries in Madeira. H & H is now the largest independent producer and shipper of Madeira Wines and the only Madeira House that owns vineyards. Still privately owned since their foundation in 1850. They also have some of the oldest reserve casks on the island ie. their 1st ever 50 years Tinta Negra'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Grape Varietal: '
      -
        type: text
        text: '100% Tinta Negra This astonishing Madeira is the product of One Single Barrel--Magical 50 Year Old Madeira to die for…'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Country of Origin: '
      -
        type: text
        text: Portugal
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Appellation:'
      -
        type: text
        text: ' The sub-tropical Atlantic island of Madeira some 800 km south-west of Portugal'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Technical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of bottle: '
              -
                type: text
                text: '750 ml bottle'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Type of Closure:'
              -
                type: text
                text: ' cork closure'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'UPC: '
              -
                type: text
                text: '5601196017367'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Analytical data:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Alcohol content:'
              -
                type: text
                text: ' 20 %vol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Residual Sugar:'
              -
                type: text
                text: ' 58.6 g/l'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Tasting Notes:'
      -
        type: text
        text: ' Deep, dark wood color and then comes the pronounced aromas of caramel, coffee, and thick-cut marmalade. Rich, sweet, and with a gorgeous balancing acidity that makes this incredible-Rare, complex, and perfectly balanced.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food Suggestion:'
      -
        type: text
        text: ' A wine of majestic quality.'
SKU:
  -
    product_variant: 750ml
    sku: '784944'
    link_override: null
    type: sku
    enabled: true
updated_by: bef84db4-bd16-4797-bf56-979f422b66c3
updated_at: 1605284585
id: 911f5ea2-226e-4b11-94b0-c3bb11856518
---
